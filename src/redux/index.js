import { configureStore } from "@reduxjs/toolkit";
import themeModeReducer from "./reducer/mode.js";
import inputFormCvReducer from "./reducer/inputFormCv.js";
import userReducer from "./reducer/user.js";

export const store = configureStore({
  reducer: {
    themeMode: themeModeReducer,
    inputFormCv: inputFormCvReducer,
    userInRedux: userReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
