import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: {
    name: "",
    isVerify: false,
    role: "",
  },
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUserRedux: (state = initialState, action) => {
      state.value.name = action.payload.name;
      state.value.isVerify = action.payload.isVerify;
      state.value.role = action.payload.role;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setUserRedux } = userSlice.actions;

export default userSlice.reducer;
