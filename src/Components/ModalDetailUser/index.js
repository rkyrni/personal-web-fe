import React from "react";

const ModalDetailUser = ({ body, setIsModalView }) => {
  return (
    // <div className="min-h-screen z-40 bg-blue-300 absolute">
    <div
      style={{ backgroundColor: "rgba(0,0,0,0.5)", zIndex: 800 }}
      className="fixed top-0 left-0 right-0 bottom-0 self-center min-h-screen flex justify-center"
    >
      <div
        className="modal-box relative self-center bg-slate-800 text-white"
        style={{
          background:
            "linear-gradient(175deg, rgba(130,156,165,1) 39%, rgba(128,128,128,1) 71%, rgba(128,128,128,1) 80%)",
        }}
      >
        <div
          onClick={() => setIsModalView(false)}
          className="btn btn-sm btn-circle absolute right-2 top-2 text-white bg-slate-700"
        >
          ✕
        </div>
        <div className="py-1">{body}</div>
      </div>
    </div>
    // </div>
  );
};

export default ModalDetailUser;
