import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { listMenuSideBar } from "./ListMenuSidebar";

const Sidebar = () => {
  const navigate = useNavigate();
  const pathName = useLocation().pathname;
  const splitterPathname = pathName.split("/");

  const ItemSidebar = ({ label, icon, link, className }) => {
    return (
      <div
        onClick={() => navigate(`/back-office/${link}`)}
        className={`${className} py-3 pl-4 hover:cursor-pointer hover:bg-slate-300 hover:text-black flex gap-2`}
      >
        <span className="self-center pt-1">{icon}</span>
        <span className="self-center">{label}</span>
      </div>
    );
  };

  const ListMenu = ({ data }) => {
    const listMenuSidebar = data.map((el, index) => {
      let localStyle = "";

      if (splitterPathname[2] === el.link) {
        localStyle =
          "border-r-4 border-blue-600 text-blue-600 font-semibold bg-slate-100 hover:text-blue-600";
      }
      return (
        <ItemSidebar
          key={index}
          label={el.label}
          link={el.link}
          icon={el.icon}
          className={localStyle}
        />
      );
    });

    return listMenuSidebar;
  };

  return (
    <div className="w-1/6 bg-white flex flex-col justify-start rounded-tr-lg rounded-br-lg shadow-lg">
      <div className="self-center py-9 border-b border-slate-200 w-full text-center mb-4 font-semibold font-serif">
        ini <strong className="text-yellow-500 text-3xl">LOGO</strong>
      </div>
      <div className="">
        <ListMenu data={listMenuSideBar} />
      </div>
    </div>
  );
};

export default Sidebar;
