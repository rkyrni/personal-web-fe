import {
  HiInboxStack,
  HiOutlineSwatch,
  HiOutlineUserPlus,
  HiUserGroup,
} from "react-icons/hi2";

const listMenuSideBar = [
  {
    label: "Home",
    link: "home",
    icon: <HiInboxStack className="h-4 w-4" />,
  },
  {
    label: "Kanban",
    link: "kanban",
    icon: <HiOutlineSwatch className="h-4 w-4" />,
  },
  {
    label: "Users",
    link: "users",
    icon: <HiUserGroup className="h-4 w-4" />,
  },
  {
    label: "Create Nasabah",
    link: "create-nasabah",
    icon: <HiOutlineUserPlus className="h-4 w-4" />,
  },
];

export { listMenuSideBar };
