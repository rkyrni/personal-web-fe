import { useSelector } from "react-redux";
import { Link, useLocation } from "react-router-dom";

const ItemMenuDropdownNavbarMobile = ({ label, icon, link, isPrivate }) => {
  const themeMode = useSelector((state) => state.themeMode.value);
  const location = useLocation().pathname;
  let styleLocation = "";
  if (location === link) {
    styleLocation = themeMode
      ? "text-yellow-500 hover:text-yellow-600 border-b border-yellow-500 mb-3"
      : "text-blue-500 hover:text-blue-600 border-b border-blue-500 mb-3";
  }
  return (
    <Link
      to={link}
      className={`${styleLocation} flex gap-1 pb-3 font-semibold text-white`}
    >
      <span className="self-center">{icon}</span>{" "}
      <span className="">{label}</span>
    </Link>
  );
};

export default ItemMenuDropdownNavbarMobile;
