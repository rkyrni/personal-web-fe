import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ImSun } from "react-icons/im";
import { IoMoon } from "react-icons/io5";
import { FaResolving } from "react-icons/fa";
import { changeMode } from "../../redux/reducer/mode";
import { dataMenuNavbar } from "./dataMenuNavbar";
import { dataMenuNavbarAlreadyLogin } from "./dataMenuAlreadyLogin";
import ItemMenuNavbar from "./ItemMenuNavbar";
import ItemMenuDropdownNavbarMobile from "./ItemMenuDropdownNavbarMobile";
import Cookies from "js-cookie";
import { useNavigate } from "react-router-dom";
import Modal from "../Modal";
import axios from "axios";
import Loading from "../Loading";

const Navbar = () => {
  const themeMode = useSelector((state) => state.themeMode.value);
  const dispatch = useDispatch();
  const [listMenu, setListMenu] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const navigate = useNavigate();

  useEffect(() => {
    if (Cookies.get("token")) setListMenu(dataMenuNavbarAlreadyLogin);
    else setListMenu(dataMenuNavbar);
  }, []);

  const handlerLogout = async () => {
    setShowModal(false);
    setIsLoading(true);
    try {
      await axios.get(`${process.env.REACT_APP_BASE_URL}/logout`, {
        headers: { Authorization: `${Cookies.get("token")}` },
      });
      Cookies.remove("token");
      navigate("/login");
    } catch (error) {
      setIsLoading(false);
      alert(error);
    }
  };

  const FooterModal = () => {
    return (
      <div className="flex justify-evenly">
        <button
          type="button"
          className="bg-slate-900 text-slate-200 px-3 py-1 rounded-lg"
          onClick={() => setShowModal(false)}
        >
          tidak
        </button>
        <button
          type="button"
          className="bg-red-500 text-slate-200 px-3 py-1 rounded-lg"
          onClick={handlerLogout}
        >
          keluar
        </button>
      </div>
    );
  };

  return (
    <>
      {isLoading ? <Loading /> : null}
      <div
        className={`${
          themeMode
            ? "bg-black shadow-white text-white"
            : "bg-white shadow-black text-slate-800"
        } shadow-md fixed top-0 left-0 right-0 rounded-b-lg`}
        style={{ zIndex: 100 }}
      >
        {showModal ? (
          <Modal
            body={"Apakah anda ingin keluar?"}
            footer={<FooterModal />}
            setIsModalView={setShowModal}
          />
        ) : null}
        <div className="flex justify-center lg:justify-between">
          <div className="hidden lg:flex pl-8">
            <div className="self-center">
              <h2
                className="text-2xl border border-red-500 rounded-full p-2 text-black"
                style={{
                  background:
                    "linear-gradient(186deg, rgba(255,132,115,1) 35%, rgba(255,249,210,1) 100%, rgba(255,249,210,1) 100%)",
                }}
              >
                <FaResolving />
              </h2>
            </div>
          </div>
          <div className="flex gap-5 lg:gap-16">
            <div className="flex gap-10 lg:gap-4">
              {listMenu?.map((item, i) => {
                return (
                  <ItemMenuNavbar
                    label={item.label}
                    key={i}
                    icon={item.icon}
                    link={item.link}
                    setShowModal={setShowModal}
                  />
                );
              })}
            </div>
            <div className="p-4 pr-0 lg:pr-6 hidden lg:block">
              {themeMode ? (
                <button
                  onClick={() => dispatch(changeMode())}
                  type="button"
                  className="text-yellow-600 hover:text-yellow-500"
                >
                  <ImSun className="w-4 h-4 lg:w-8 lg:h-8" />
                </button>
              ) : (
                <button
                  onClick={() => dispatch(changeMode())}
                  type="button"
                  className="text-yellow-600 hover:text-yellow-500"
                >
                  <IoMoon className="w-4 h-4 lg:w-8 lg:h-8" />
                </button>
              )}
            </div>
            <div className="dropdown lg:hidden block">
              <label tabIndex={0} className="btn btn-ghost btn-circle">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-5 w-5"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M4 6h16M4 12h16M4 18h7"
                  />
                </svg>
              </label>
              <ul
                tabIndex={0}
                className="menu menu-compact dropdown-content mt-3 p-2 shadow bg-slate-800 text-slate-200 rounded-box w-52 -ml-36"
              >
                {listMenu?.map((item, i) => {
                  if (item.label === "Keluar") {
                    return (
                      <li key={i}>
                        <button
                          type="button"
                          onClick={handlerLogout}
                          className={`flex gap-1 pb-3 font-semibold`}
                        >
                          <span className="self-center text-red-500">
                            {item.icon}
                          </span>{" "}
                          <span className="text-white">{item.label}</span>
                        </button>
                      </li>
                    );
                  }

                  return (
                    <li key={i} className="flex justify-start">
                      <ItemMenuDropdownNavbarMobile
                        label={item.label}
                        icon={item.icon}
                        link={item.link}
                      />
                    </li>
                  );
                })}
                <li>
                  <button
                    type="button"
                    onClick={() => dispatch(changeMode())}
                    className={`flex gap-1 pb-3 font-semibold`}
                  >
                    <span className="self-center text-yellow-500">
                      {themeMode ? <ImSun /> : <IoMoon />}
                    </span>{" "}
                    <span className="text-white">
                      {themeMode ? "Mode Terang" : "Mode Gelap"}
                    </span>
                  </button>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Navbar;
