import { BsController } from "react-icons/bs";
import {
  HiSquares2X2,
  HiArrowRightOnRectangle,
  HiUserCircle,
  HiUserGroup,
} from "react-icons/hi2";

const dataMenuNavbarAlreadyLogin = [
  {
    label: "Beranda",
    link: "/home",
    icon: <HiSquares2X2 />,
    isPrivate: false,
  },
  {
    label: "Game",
    link: "/game-list",
    icon: <BsController />,
    isPrivate: false,
  },
  {
    label: "Profil",
    link: "/profile",
    icon: <HiUserCircle />,
    isPrivate: false,
  },
  {
    label: "User",
    link: "/list-users",
    icon: <HiUserGroup />,
    isPrivate: false,
  },
  {
    label: "Keluar",
    link: "-",
    icon: <HiArrowRightOnRectangle />,
    isPrivate: false,
  },
];

export { dataMenuNavbarAlreadyLogin };
