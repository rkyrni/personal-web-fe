import { VscCode } from "react-icons/vsc";
import { BsController } from "react-icons/bs";
import {
  HiArrowLeftOnRectangle,
  HiSquares2X2,
  HiUserGroup,
} from "react-icons/hi2";

const dataMenuNavbar = [
  {
    label: "Beranda",
    link: "/home",
    icon: <HiSquares2X2 />,
    isPrivate: false,
  },
  {
    label: "Game",
    link: "/game-list",
    icon: <BsController />,
    isPrivate: false,
  },
  {
    label: "User",
    link: "/list-users",
    icon: <HiUserGroup />,
    isPrivate: false,
  },
  {
    label: "Tentang Developer",
    link: "/",
    icon: <VscCode />,
    isPrivate: false,
  },
  {
    label: "Masuk/Daftar",
    link: "/login",
    icon: <HiArrowLeftOnRectangle />,
    isPrivate: false,
  },
];

export { dataMenuNavbar };
