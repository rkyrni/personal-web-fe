import { useSelector } from "react-redux";
import { Link, useLocation } from "react-router-dom";

const ItemMenuNavbar = ({ label, icon, link, isPrivate, setShowModal }) => {
  const themeMode = useSelector((state) => state.themeMode.value);
  const location = useLocation().pathname;
  let styleLocation = "";
  if (location === link) {
    styleLocation = themeMode
      ? "text-yellow-500 hover:text-yellow-600 border-b border-yellow-500"
      : "text-blue-500 hover:text-blue-600 border-b border-blue-500";
  }

  if (label === "Keluar") {
    return (
      <button
        onClick={() => setShowModal(true)}
        className={`${styleLocation} self-center flex gap-1 pb-3 text-sm`}
      >
        <span className="self-center">{icon}</span>
        <span className="hidden lg:block">{label}</span>
      </button>
    );
  }
  return (
    <Link
      to={link}
      className={`${styleLocation} self-center flex gap-1 pb-3 text-sm`}
    >
      <span className="self-center block lg:hidden">{icon}</span>
      <span className="hidden lg:block">{label}</span>
    </Link>
  );
};

export default ItemMenuNavbar;
