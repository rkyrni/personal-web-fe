import React from "react";

const Modal = ({ tittle, body, footer, setIsModalView }) => {
  return (
    // <div className="min-h-screen z-40 bg-blue-300 absolute">
    <div
      style={{ backgroundColor: "rgba(0,0,0,0.5)", zIndex: 800 }}
      className="fixed top-0 left-0 right-0 bottom-0 self-center min-h-screen flex justify-center"
    >
      <div className="modal-box relative self-center bg-slate-800 text-white">
        <div
          onClick={() => setIsModalView(false)}
          className="btn btn-sm btn-circle absolute right-2 top-2 text-white bg-slate-700"
        >
          ✕
        </div>
        <h3 className="text-lg font-bold">{tittle}</h3>
        <div className="py-4">{body}</div>
        <div className="py-4">{footer}</div>
      </div>
    </div>
    // </div>
  );
};

export default Modal;
