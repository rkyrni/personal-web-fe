import React from "react";
import {
  Routes,
  Route,
  HashRouter,
  BrowserRouter,
  Navigate,
} from "react-router-dom";
import ChooseTemplate from "../Pages/ChooseTemplate";
import GameList from "../Pages/GameList";
import InputForm from "../Pages/InputForm";
import TemplatePDF from "../Pages/TemplatePDF";
import ValidationWorkExperience from "../Pages/ValidationWorkExperience";
import WorkExperienceForm from "../Pages/WorkExperienceForm";
import Landing from "../Pages/Landing";
import Register from "../Pages/Register";
import Login from "../Pages/Login";
import Profile from "../Pages/Profile";
import Cookies from "js-cookie";
import EditProfile from "../Pages/EditProfile";
import GameDetail from "../Pages/GameDetail";
import BatuGuntingKertas from "../Pages/PlayGame/BatuGuntingKertas";
import TebakAngka from "../Pages/PlayGame/TebakAngka";
import AboutDeveloper from "../Pages/AboutDeveloper";
import NotFoundPage from "../Pages/NotFoundPage";
import HomeBo from "../Pages/BackOffice/HomeBo";
import Users from "../Pages/BackOffice/Users";
import ListUsers from "../Pages/ListUsers";

function Router() {
  const PrivateRoute = ({ content }) => {
    if (Cookies.get("token")) return content;
    else return <Navigate to="/home" />;
  };

  const HandlerLoginRegister = ({ content }) => {
    if (Cookies.get("token")) return <Navigate to="/home" />;
    else return content;
  };

  return (
    <HashRouter>
      <Routes>
        <Route path="/" element={<AboutDeveloper />} />
        <Route path="/home" element={<Landing />} />
        <Route
          path="/register"
          element={<HandlerLoginRegister content={<Register />} />}
        />
        <Route
          path="/login"
          element={<HandlerLoginRegister content={<Login />} />}
        />
        <Route
          path="/profile"
          element={<PrivateRoute content={<Profile />} />}
        />

        <Route
          path="/edit-profile"
          element={<PrivateRoute content={<EditProfile />} />}
        />

        <Route path="/game-detail/:label" element={<GameDetail />} />

        <Route
          path="/play-game/batu-gunting-kertas"
          element={<PrivateRoute content={<BatuGuntingKertas />} />}
        />
        <Route
          path="/play-game/tebak-angka"
          element={<PrivateRoute content={<TebakAngka />} />}
        />

        <Route path="/pilih-template" element={<ChooseTemplate />} />
        <Route path="/input-form" element={<InputForm />} />
        <Route
          path="/Pengalaman-kerja"
          element={<ValidationWorkExperience />}
        />
        <Route path="/form-pengalaman-kerja" element={<WorkExperienceForm />} />
        <Route path="/download-cv" element={<TemplatePDF />} />
        <Route path="/list-users" element={<ListUsers />} />
        <Route path="/game-list" element={<GameList />} />
        <Route path="/back-office/home" element={<HomeBo />} />
        <Route path="/back-office/users" element={<Users />} />

        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </HashRouter>
  );
}

export default Router;
