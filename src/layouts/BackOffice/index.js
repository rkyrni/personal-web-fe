import React from "react";
import Sidebar from "../../Components/Sidebar";

const BackOfficeTemplate = ({ children }) => {
  return (
    <div className="flex gap-8 min-h-screen bg-slate-100">
      <Sidebar />
      <div className="bg-slate-100 w-full">{children}</div>
    </div>
  );
};

export default BackOfficeTemplate;
