import React, { useEffect, useState } from "react";
import Navbar from "../../Components/Navbar";
import DefaultProfile from "../../assets/img/default-profile.png";
import axios from "axios";
import Cookies from "js-cookie";
import Loading from "../../Components/Loading";
import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { HiTrash } from "react-icons/hi2";

const EditProfile = () => {
  const [imageFile, setImageFile] = useState(null);
  const [isDeleteProfile, setIsDeleteProfile] = useState(false);
  const [sendFile, setSendFile] = useState(null);
  const [user, setUser] = useState(null);
  const [fecth, setFetch] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [input, setInput] = useState({
    name: "",
    bio: "",
    profileImage: "",
  });

  const navigate = useNavigate();
  const themeMode = useSelector((state) => state.themeMode.value);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_BASE_URL}/user`,
          {
            headers: { Authorization: `${Cookies.get("token")}` },
          }
        );
        setUser(response.data);
        setInput({ name: response.data.name, bio: response.data.bio });
        setIsLoading(false);
      } catch (error) {
        alert(error);
        setIsLoading(false);
      }
    };

    if (fecth) {
      fetchData();
      setFetch(false);
    }
  }, [fecth, user]);

  const getFormData = (object) => {
    const formData = new FormData();
    Object.keys(object).forEach((key) => formData.append(key, object[key]));
    return formData;
  };

  const handlerSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);

    try {
      if (sendFile) input.profileImage = sendFile;
      if (!sendFile && isDeleteProfile) input.isDeleteProfile = true;
      else input.isDeleteProfile = false;

      const payloadFormData = getFormData(input);
      await axios.post(
        `${process.env.REACT_APP_BASE_URL}/edit-profile`,
        sendFile ? payloadFormData : input,
        {
          headers: { Authorization: `${Cookies.get("token")}` },
        }
      );
      setIsLoading(false);
      navigate("/profile");
    } catch (error) {
      alert(error);
      setIsLoading(false);
    }
  };

  const handlerChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  return (
    <div>
      {/* <Navbar /> */}
      {isLoading ? <Loading /> : null}
      <form
        method="POST"
        onSubmit={handlerSubmit}
        className={`${
          themeMode ? "bg-black text-slate-200" : "bg-white text-slate-800"
        } p-2`}
      >
        <div
          className={`${
            themeMode
              ? "bg-slate-800 shadow-md shadow-white"
              : "bg-slate-100 shadow-lg"
          } rounded flex flex-col pt-16 min-h-screen gap-5`}
        >
          <div className="self-center flex flex-col gap-5 mb-4 relative">
            {imageFile || user?.imageURL ? (
              <button
                onClick={() => {
                  if (user.imageURL) {
                    setUser({ ...user, imageURL: "" });
                    setIsDeleteProfile(true);
                  }
                  setImageFile(null);
                  setSendFile(null);
                }}
                className="absolute top-2 right-2 bg-slate-400 p-1 rounded-full z-10"
              >
                <HiTrash />
              </button>
            ) : null}
            <label htmlFor="profileImage" className="cursor-pointer">
              <img
                src={
                  imageFile
                    ? imageFile
                    : user?.imageURL
                    ? user?.imageURL
                    : DefaultProfile
                }
                alt="profile"
                className="w-[100px] h-[100px] lg:w-[200px] lg:h-[200px] self-center"
              />
            </label>
            <input
              id="profileImage"
              type="file"
              className="hidden"
              name="profileImage"
              onChange={(e) => {
                setSendFile(e.target.files[0]);
                const [file] = e.target.files;
                if (file) {
                  setImageFile(URL.createObjectURL(file));
                }
              }}
            />
          </div>
          <div className="px-5 flex flex-col gap-3 lg:self-center">
            <div className="flex gap-2">
              <p className="text-xs self-center">Nama :</p>
              <input
                className={`border-b ${
                  themeMode
                    ? "bg-slate-800 border-white"
                    : "bg-slate-100 border-slate-800"
                } text-xs italic text-slate-400 px-2 py-1`}
                type="text"
                value={input?.name}
                placeholder="nama"
                name="name"
                onChange={handlerChange}
              />
            </div>
            <div className="flex gap-2 mb-6">
              <p className="text-xs self-center">Bio :</p>
              <input
                className={`border-b ${
                  themeMode
                    ? "bg-slate-800 border-white"
                    : "bg-slate-100 border-slate-800"
                } text-xs italic text-slate-400 px-2 py-1`}
                type="text"
                value={input?.bio ? input.bio : ""}
                placeholder="bio..."
                name="bio"
                onChange={handlerChange}
              />
            </div>
          </div>
          <div className="flex justify-end pr-6 gap-10 lg:pr-96">
            <Link
              to="/profile"
              className={`${
                themeMode ? "border border-white" : "border border-black"
              } rounded-lg py-1 px-2 text-xs`}
            >
              batal
            </Link>
            <button
              type="submit"
              className={`${
                themeMode ? "border border-white" : "border border-black"
              } rounded-lg py-1 px-2 text-xs`}
            >
              simpan
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default EditProfile;
