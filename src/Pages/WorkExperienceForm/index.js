import Cookies from "js-cookie";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { deletedWorkExperience } from "../../redux/reducer/inputFormCv";
import Input from "./Input";

const WorkExperienceForm = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const inputFormCv = useSelector((state) => state.inputFormCv.value);

  const handleSubmit = (e) => {
    e.preventDefault();
    Cookies.set("inputFormCv", JSON.stringify(inputFormCv));
    navigate("/pilih-template");
  };

  return (
    <div className="bg-slate-200 min-h-screen p-4 flex justify-center">
      <form
        action="POST"
        method="POST"
        onSubmit={handleSubmit}
        className="bg-white rounded-lg shadow-lg self-center p-4 flex flex-col gap-5"
      >
        <div className="flex flex-col gap-5">
          <h2 className="font-bold text-slate-800 text-center">
            Pengalaman Kerja
          </h2>
          <Input />
        </div>
        <div className="flex flex-col gap-5">
          <button
            type="submit"
            className="w-full bg-green-500 py-2 text-white font-semibold rounded-lg shadow-lg"
          >
            Submit
          </button>
          <button
            type="button"
            onClick={() => {
              dispatch(deletedWorkExperience());
              const tempInputObj = JSON.parse(Cookies.get("inputFormCv"));
              const newInput = {
                ...tempInputObj,
                companyName1: "",
                companyName2: "",
                companyName3: "",
                position1: "",
                position2: "",
                position3: "",
                fromWorkExperience1: "",
                fromWorkExperience2: "",
                fromWorkExperience3: "",
                toWorkExperience1: "",
                toWorkExperience2: "",
                toWorkExperience3: "",
              };
              Cookies.set("inputFormCv", JSON.stringify(newInput));
              navigate("/pilih-template");
            }}
            className="text-xs text-blue-500 italic self-end underline"
          >
            Tidak punya pengalaman kerja!
          </button>
        </div>
      </form>
    </div>
  );
};

export default WorkExperienceForm;
