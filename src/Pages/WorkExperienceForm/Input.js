import React, { useEffect, useState } from "react";
import { HiOutlineTrash, HiOutlinePlus } from "react-icons/hi2";
import { useDispatch, useSelector } from "react-redux";
import { onChangeInput } from "../../redux/reducer/inputFormCv";

const Input = () => {
  const [inputLength, setInputLength] = useState(1);
  const [validationInput, setValidationInput] = useState(false);

  const inputFormCv = useSelector((state) => state.inputFormCv.value);
  const dispatch = useDispatch();

  const result = [];

  useEffect(() => {
    if (inputFormCv.companyName6) setInputLength(6);
    else if (inputFormCv.companyName5) setInputLength(5);
    else if (inputFormCv.companyName4) setInputLength(4);
    else if (inputFormCv.companyName3) setInputLength(3);
    else if (inputFormCv.companyName2) setInputLength(2);
  }, []);

  const handleDeleteInput = () => {
    setInputLength(inputLength - 1);
  };

  const handleAddInput = (i) => {
    if (
      !inputFormCv[`companyName${i}`] ||
      !inputFormCv[`position${i}`] ||
      !inputFormCv[`fromWorkExperience${i}`] ||
      !inputFormCv[`toWorkExperience${i}`]
    ) {
      setValidationInput(true);
    } else {
      setInputLength(inputLength + 1);
      setValidationInput(false);
    }
  };

  for (let i = 1; i <= inputLength; i++) {
    let temp = (
      <div key={i} className="flex flex-col gap-2 mb-4">
        <p className="text-slate-800 text-xs font-semibold">
          Pengalaman Kerja {i} :
        </p>
        <div className="flex justify-between gap-2">
          <p className="text-xs text-slate-700 self-center">
            Nama Perusahaan :
          </p>
          <input
            type="text"
            required={i === 1}
            placeholder={"PT Contoh..."}
            name={`companyName${i}`}
            value={inputFormCv[`companyName${i}`]}
            onChange={(e) => dispatch(onChangeInput(e.target))}
            className={`${
              validationInput && i === inputLength
                ? "border-red-500"
                : "border-yellow-500"
            } bg-slate-200 rounded border text-slate-800 px-2`}
          />
        </div>
        <div className="flex justify-between gap-2">
          <p className="text-xs text-slate-700 self-center">Jabatan :</p>
          <input
            type="text"
            required={i === 1}
            placeholder={"Office Boy"}
            name={`position${i}`}
            value={inputFormCv[`position${i}`]}
            onChange={(e) => dispatch(onChangeInput(e.target))}
            className={`${
              validationInput && i === inputLength
                ? "border-red-500"
                : "border-yellow-500"
            } bg-slate-200 rounded border text-slate-800 px-2`}
          />
        </div>
        <div className="flex justify-between gap-2">
          <p className="text-xs text-slate-700 self-center">Dari :</p>
          <input
            type="date"
            required={i === 1}
            name={`fromWorkExperience${i}`}
            value={inputFormCv[`fromWorkExperience${i}`]}
            onChange={(e) => dispatch(onChangeInput(e.target))}
            className={`${
              validationInput && i === inputLength
                ? "border-red-500"
                : "border-yellow-500"
            } bg-slate-200 rounded border text-slate-800 px-2`}
          />
        </div>
        <div className="flex justify-between gap-2 mb-2">
          <p className="text-xs text-slate-700 self-center">Sampai :</p>
          <input
            type="date"
            required={i === 1}
            name={`toWorkExperience${i}`}
            value={inputFormCv[`toWorkExperience${i}`]}
            onChange={(e) => dispatch(onChangeInput(e.target))}
            className={`${
              validationInput && i === inputLength
                ? "border-red-500"
                : "border-yellow-500"
            } bg-slate-200 rounded border text-slate-800 px-2`}
          />
        </div>
        <div className="flex pl-8 gap-5">
          {i !== 1 && i === inputLength ? (
            <button
              onClick={handleDeleteInput}
              type="button"
              className="bg-slate-700 p-2 rounded-full text-white"
            >
              <HiOutlineTrash />
            </button>
          ) : null}
          {i < 3 && i === inputLength ? (
            <button
              type="button"
              onClick={() => handleAddInput(i)}
              className="border border-slate-700 p-2 rounded-full text-black"
            >
              <HiOutlinePlus />
            </button>
          ) : null}
        </div>
      </div>
    );

    result.push(temp);
  }

  return result;
};

export default Input;
