import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { deletedWorkExperience } from "../../redux/reducer/inputFormCv";

const ValidationWorkExperience = () => {
  const [name, setName] = useState(null);

  const inputFormCv = useSelector((state) => state.inputFormCv.value);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    if (Cookies.get("inputForm")) setName(Cookies.get("inputForm"));
  }, [name]);

  return (
    <div className="bg-slate-200 flex min-h-screen p-4">
      <div className="bg-white rounded-lg shadow-lg w-full flex justify-center">
        <div className="bg-slate-200 rounded-lg self-center p-3">
          <h2 className="text-2xl text-green-500 font-bold">
            Hi, {name ? name : "User"}!
          </h2>
          <h2 className="text-slate-800 font-semibold mb-8">
            Apakah anda memiliki pengalaman kerja ?
          </h2>
          <div className="flex justify-around">
            <button
              type="button"
              onClick={() => {
                dispatch(deletedWorkExperience());
                const tempInputObj = JSON.parse(Cookies.get("inputFormCv"));
                const newInput = {
                  ...tempInputObj,
                  companyName1: "",
                  companyName2: "",
                  companyName3: "",
                  position1: "",
                  position2: "",
                  position3: "",
                  fromWorkExperience1: "",
                  fromWorkExperience2: "",
                  fromWorkExperience3: "",
                  toWorkExperience1: "",
                  toWorkExperience2: "",
                  toWorkExperience3: "",
                };
                Cookies.set("inputFormCv", JSON.stringify(newInput));
                navigate("/pilih-template");
              }}
              className="bg-red-500 py-2 px-7 rounded-lg shadow text-black font-semibold"
            >
              tidak
            </button>
            <button
              onClick={() => navigate("/form-pengalaman-kerja")}
              type="button"
              className="bg-green-500 py-2 px-7 rounded-lg shadow text-white font-semibold"
            >
              ya
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ValidationWorkExperience;
