import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import Navbar from "../../Components/Navbar";
import bg1 from "../../assets/img/bg-1.jpg";
import logo from "../../assets/img/logo.png";
import image3 from "../../assets/img/image-3.jpg";
import Footer from "../../Components/Footer";

const Landing = () => {
  const themeMode = useSelector((state) => state.themeMode.value);
  const navigate = useNavigate();

  return (
    <div className={`${themeMode ? "bg-black" : "bg-slate-200"} min-h-screen`}>
      <Navbar />
      <div
        className="pt-14 bg-center bg-cover bg-no-repeat min-h-screen"
        style={{ backgroundImage: `url(${bg1})` }}
      >
        <div className="flex flex-col gap-2">
          {/* Header */}
          <div className="px-1">
            <div
              className={`text-yellow-200 rounded-lg shadow-sm flex justify-evenly py-24 px-2 bg-cover bg-center bg-no-repeat`}
              style={{ backgroundImage: `url(${image3})` }}
            >
              <div className="font-serif self-center text-center">
                <p className="text-md font-bold lg:text-2xl">
                  The more you learn, The more you earn
                </p>
                <p className="text-xs lg:text-xl">
                  Waktu yang lewat tidak akan pernah kembali!
                </p>
              </div>
            </div>
          </div>
          <div className="">
            {/* text */}
            <div
              className={`${
                themeMode
                  ? "bg-black text-white"
                  : "bg-slate-200 text-slate-800"
              } text-center py-5 italic mb-2`}
            >
              <p>Apa Saja Fitur di Web App ini?</p>
            </div>
            <div className="flex flex-col gap-2 px-1">
              {/* Sec 1 */}
              <div
                className={`${
                  themeMode
                    ? "bg-black text-slate-100"
                    : "bg-slate-200 text-slate-800"
                } px-1 py-3 rounded-lg shadow-sm`}
              >
                <div className="flex flex-col gap-3 px-3">
                  <div>
                    <h2 className="text-center text-xl font-semibold mb-5">
                      Membuat Resume(CV)
                    </h2>
                    <p className="text-xs lg:text-sm px-5 text-start lg:text-center">
                      Buat resume(cv) kurang dari 5 menit dengan pilihan
                      template yang telah tersedia.
                    </p>
                  </div>
                  <div className="flex justify-center mb-4">
                    <ul className="steps steps-horizontal text-xs z-10">
                      <li className="step step-warning">Isi Form</li>
                      <li className="step step-warning">Pilih Template</li>
                      <li className="step">Download</li>
                    </ul>
                  </div>
                  <div className="flex">
                    <Link
                      to="/input-form"
                      className="bg-yellow-500 text-slate-800 font-bold text-center py-2 w-full rounded hover:bg-yellow-600"
                    >
                      Buat Resume
                    </Link>
                  </div>
                </div>
              </div>
              {/* Sec 2 */}
              <div
                className={`${
                  themeMode
                    ? "bg-black text-slate-100"
                    : "bg-slate-200 text-slate-800"
                } px-1 py-3 rounded-lg shadow-sm`}
              >
                <div className="flex flex-col gap-3 px-3">
                  <div>
                    <h2 className="text-center text-xl font-semibold mb-5">
                      Bermain Game
                    </h2>
                    <p className="text-xs lg:text-sm px-5 text-start lg:text-center">
                      Bermain game klasik sederhana dengan menampilkan skor
                      permainan dan skor tertinggimu. Kamu juga dapat melihat
                      papan peringkat dari setiap game.
                    </p>
                  </div>
                  <div className="flex justify-evenly mb-5 mt-4">
                    <img
                      src={logo}
                      alt="logo"
                      className="w-12 h-10 animate-spin"
                      style={{ animationDuration: "5000ms" }}
                    />
                    <img
                      src={logo}
                      alt="logo"
                      className="w-12 h-10 animate-spin"
                      style={{ animationDuration: "5000ms" }}
                    />
                    <img
                      src={logo}
                      alt="logo"
                      className="w-12 h-10 animate-spin"
                      style={{ animationDuration: "5000ms" }}
                    />
                  </div>
                  <div className="flex">
                    <Link
                      to={"/game-list"}
                      className="bg-yellow-500 text-slate-800 font-bold text-center py-2 w-full rounded hover:bg-yellow-600"
                    >
                      Mainkan Game
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="pt-2">
          <Footer />
        </div>
      </div>
    </div>
  );
};

export default Landing;
