import React, { useState } from "react";
import axios from "axios";
import Cookies from "js-cookie";
import { io } from "socket.io-client";
import {
  HiOutlineEnvelopeOpen,
  HiOutlineLockClosed,
  HiUser,
  HiOutlineArrowLeft,
} from "react-icons/hi2";
import { Link, useNavigate } from "react-router-dom";
import Loading from "../../Components/Loading";
import Background from "../../assets/img/bg-4.jpg";
import { useDispatch, useSelector } from "react-redux";
import { setUserRedux } from "../../redux/reducer/user";

const socket = io(process.env.REACT_APP_BASE_URL);

const Login = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [textHeader, setTextHeader] = useState("Login Form");
  const [input, setInput] = useState({
    email: "",
    password: "",
  });

  const navigate = useNavigate();
  const userInRedux = useSelector((state) => state.userInRedux.value);
  const dispatch = useDispatch();

  const textManipulation = () => {
    setTimeout(() => {
      setTextHeader("Login Form");
    }, 5000);
  };

  const handlerChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  const handlerSubmit = async (e) => {
    e.preventDefault();
    input.email = input.email.toLowerCase();
    setIsLoading(true);
    try {
      const response = await axios.post(
        `${process.env.REACT_APP_BASE_URL}/login`,
        input
      );
      Cookies.set("token", response.data.data.accessToken);
      Cookies.set("name", response.data.data.name);
      dispatch(setUserRedux(response.data.data));
      navigate("/home");
      // socket.emit("login_logout", "Login");
      // navigate("/profile");
      setIsLoading(false);
    } catch (error) {
      console.log(error.response.status);
      if (error.response.status === 403) {
        setTextHeader("Email Salah");
        textManipulation();
        setIsLoading(false);
        return;
      }
      if (error.response.status === 402) {
        setTextHeader("Password Salah");
        textManipulation();
        setIsLoading(false);
        return;
      }
      alert(error);
    }
  };

  return (
    <div
      style={{ backgroundImage: `url(${Background})` }}
      className="flex justify-center min-h-screen bg-cover"
    >
      {isLoading ? <Loading /> : null}
      <div
        style={{
          boxShadow: "0px 0px 25px 10px black",
          background: "rgba(4, 29, 23, 0.5)",
        }}
        className="self-center py-3 px-8 rounded-lg flex flex-col gap-5 lg:w-1/4 w-3/4"
      >
        <div className="flex justify-center -mt-8">
          <div
            style={{ boxShadow: "0px 0px 6px 5px black" }}
            className="w-max p-3 rounded-full bg-black text-white"
          >
            <HiUser />
          </div>
        </div>
        <Link to={"/home"}>
          <HiOutlineArrowLeft className="w-[30px] h-[30px]" color={"#f59e0b"} />
        </Link>
        <h2
          className={`${
            textHeader !== "Login Form" ? "text-red-500" : "text-slate-200"
          } text-2xl text-center font-bold mb-6 animate-bounce`}
        >
          {textHeader}
        </h2>
        <form
          className="flex flex-col gap-6"
          action=""
          method="post"
          onSubmit={handlerSubmit}
        >
          <div className="flex gap-3 border-b-2 border-slate-700 pb-1">
            <span className="self-center">
              <HiOutlineEnvelopeOpen className="text-white" />
            </span>
            <input
              className="bg-transparent text-white font-semibold px-2"
              name="email"
              onChange={handlerChange}
              type="email"
              placeholder="email"
              value={input.name}
            />
          </div>
          <div className="flex gap-3 border-b-2 border-slate-700 pb-1">
            <span className="self-center">
              <HiOutlineLockClosed className="text-white" />
            </span>
            <input
              className="bg-transparent text-white font-semibold px-2"
              name="password"
              onChange={handlerChange}
              type="password"
              placeholder="password"
              value={input.password}
            />
          </div>
          <button
            className="border border-slate-700 py-2 font-semibold hover:shadow-2xl hover:shadow-black text-white"
            type="submit"
          >
            Login
          </button>
        </form>
        <div className="flex text-slate-200 gap-1 justify-end">
          <p>Belum punya akun?, Daftar </p>
          <Link className="italic text-blue-400" to="/register">
            disini!
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Login;
