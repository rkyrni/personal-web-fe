import React from "react";
import Template1 from "../../assets/img/template-1.png";
import Template2 from "../../assets/img/template-2.png";
import InputChooseTemplate from "./InputChooseTemplate";

const ChooseTemplate = () => {
  return (
    <div className="bg-slate-200 min-h-screen px-2 pt-3 pb-10 flex flex-col gap-5">
      <div>
        <h2 className="font-bold text-2xl text-yellow-500 text-center">
          Pilih Template
        </h2>
      </div>
      <div className="flex flex-col gap-6 lg:flex-row lg:justify-center">
        <div className="bg-white rounded-lg p-4 shadow-md flex flex-col gap-2 lg:h-[600px] h-[200px]">
          <img
            src={Template2}
            className="lg:h-[500px] h-[170px] w-[100px] lg:w-[300px] self-center"
            alt="tmplate-1"
          />
          <h2 className="text-center text-slate-800 text-xs">
            <strong>Nama Template :</strong> Sky Blue
          </h2>
        </div>
        <div className="bg-white rounded-lg p-4 shadow-md flex flex-col gap-2 lg:h-[600px] h-[200px]">
          <img
            src={Template1}
            className="lg:h-[500px] h-[170px] w-[100px] lg:w-[300px] self-center"
            alt="tmplate-2"
          />
          <h2 className="text-center text-slate-800 text-xs">
            <strong>Nama Template :</strong> Basic Orange
          </h2>
        </div>
      </div>
      <div>
        <InputChooseTemplate />
      </div>
    </div>
  );
};

export default ChooseTemplate;
