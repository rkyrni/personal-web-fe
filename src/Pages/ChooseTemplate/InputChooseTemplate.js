import Cookies from "js-cookie";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

const InputChooseTemplate = () => {
  const [inputChooseTemplate, setInputChooseTemplate] = useState({
    template: "",
  });

  const navigate = useNavigate();

  const handleChange = (e) => {
    setInputChooseTemplate({ template: e.target.value });
  };

  const handleSubmit = () => {
    Cookies.set("template", inputChooseTemplate.template);
    navigate("/download-cv");
  };

  return (
    <div className="flex flex-col gap-4 text-xs lg:text-sm lg:pt-10">
      <div className="flex gap-2 lg:pl-10">
        <input
          type="radio"
          id="template-1"
          value={"TEMPLATE-1"}
          name="template"
          onChange={handleChange}
          className="radio radio-info w-5 h-5"
        />
        <label className="font-semibold text-black" htmlFor="template-1">
          Sky Blue
        </label>
      </div>
      <div className="flex gap-2 lg:pl-10">
        <input
          type="radio"
          id="template-2"
          value={"TEMPLATE-2"}
          name="template"
          onChange={handleChange}
          className="radio radio-info w-5 h-5"
        />
        <label className="font-semibold text-black" htmlFor="template-2">
          Basic Orange
        </label>
      </div>
      <div className="flex">
        <button
          onClick={handleSubmit}
          disabled={inputChooseTemplate.template === ""}
          className="btn btn-active btn-accent px-3 py-2 w-full"
        >
          Lanjut
        </button>
      </div>
    </div>
  );
};

export default InputChooseTemplate;
