import axios from "axios";
import React, { useEffect, useState } from "react";
import { BsSearch } from "react-icons/bs";
import { HiExclamationCircle } from "react-icons/hi2";
import { IoIosRadioButtonOn, IoMdTrash } from "react-icons/io";
import { useNavigate, useSearchParams } from "react-router-dom";
import moment from "moment";
import momentTz from "moment-timezone";
import Loading from "../../Components/Loading";
import ModalDetailUser from "../../Components/ModalDetailUser";
import DefaultPic from "../../assets/img/default-profile.png";
import Navbar from "../../Components/Navbar";

const ListUsers = () => {
  const [fetchData, setfetchData] = useState(true);
  const [users, setUsers] = useState(null);
  const [pagination, setPagination] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [modalDetail, setModalDetail] = useState(false);
  const [detailId, setDetailId] = useState(null);
  const [searchInput, setSearchInput] = useState("");

  const [searchParams, setSearchParams] = useSearchParams();
  const navigate = useNavigate();

  useEffect(() => {
    if (searchParams.get("name")) setSearchInput(searchParams.get("name"));
    const pages = searchParams.get("page") || 1;
    const name = searchParams.get("name");
    const fetch = async () => {
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_BASE_URL}/users-page${
            name ? `?name=${name}&page=${pages}` : `?page=${pages}`
          }`
        );
        setPagination(response.data.pagination);
        setUsers(response.data.data);
        setIsLoading(false);
      } catch (error) {
        alert(error);
        setIsLoading(false);
      }
    };

    if (fetchData) {
      fetch();
      setfetchData(false);
    }
  }, [fetchData]);

  const handleSearch = async (e) => {
    e.preventDefault();
    navigate(`/list-users?name=${searchInput}&page=1`);
    setIsLoading(true);
    setfetchData(true);
  };

  const hanlderTextSearch = (params) => {
    const TextTrue = ({ char }) => {
      return <span className="text-red-500 font-bold">{char}</span>;
    };
    const TextFalse = ({ char }) => {
      return <span className="text-slate-800">{char}</span>;
    };

    const result = params.split("").map((item, i) => {
      if (searchParams.get("name")?.toLowerCase().includes(item.toLowerCase()))
        return <TextTrue key={i} char={item} />;
      else return <TextFalse key={i} char={item} />;
    });

    return result;
  };

  const BodyModalDetail = () => {
    if (users && detailId) {
      const userDetail = users.filter((item) => {
        return item.id === detailId;
      });

      const [user] = userDetail;

      return (
        <div>
          <figure className="flex justify-center mb-2">
            <img
              src={user.imageURL ? user.imageURL : DefaultPic}
              alt="pic"
              className="w-[100px] h-[100px] rounded-full"
            />
          </figure>
          <div className="flex flex-col">
            <h2 className="self-center font-bold text-xl">{user.name}</h2>
            <p className="self-center">{user.email}</p>
            <p className="self-center">{user.bio ? user.bio : "-"}</p>
            {user.isOnline ? (
              <p className="text-green-500 self-center font-bold">online</p>
            ) : (
              <p className="self-center">
                Active{" "}
                {moment(
                  momentTz(user.logoutAt).tz("Asia/Jakarta").format(),
                  "YYYY-MM-DD h:mm:s"
                ).fromNow()}
              </p>
            )}
            <p className="self-center">
              Joined{" "}
              {moment(
                momentTz(user.createdAt).tz("Asia/Jakarta").format(),
                "YYYY-MM-DD h:mm:s"
              ).fromNow()}
            </p>
          </div>
        </div>
      );
    }
  };

  const handlingChar = (param) => {
    let result = "";
    for (let j = 0; j < param.length; j++) {
      if (j < 11) result += param[j];
      if (j === 11) {
        result += "...";
        break;
      }
    }
    return result;
  };

  const Itemlist = ({ index, name, status, id }) => {
    return (
      <tr>
        <th className="py-4 text-xs text-slate-800 border-y border-black">
          {index}
        </th>
        <td className="py-4 text-xs text-slate-800 border-y border-black">
          {hanlderTextSearch(handlingChar(name))}
        </td>
        <td className="py-4 text-xs text-slate-800 border-y border-black">
          {status}
        </td>
        <td className="py-4 text-xs text-slate-800 border-y border-black">
          <button
            type="button"
            className="text-blue-500 border border-blue-500 text-xs px-2 rounded-xl shadow-lg font-semibold"
            onClick={() => {
              setDetailId(id);
              setModalDetail(true);
            }}
          >
            detail
          </button>
        </td>
      </tr>
    );
  };

  return (
    <div>
      <Navbar />
      {isLoading ? <Loading /> : null}
      {modalDetail ? (
        <ModalDetailUser
          body={<BodyModalDetail />}
          setIsModalView={setModalDetail}
          tittle={"Detail user"}
        />
      ) : null}
      <div
        className="bg-white h-full shadow-lg rounded-xl min-h-screen"
        style={{
          background:
            "linear-gradient(173deg, rgba(255,132,115,1) 35%, rgba(255,249,210,1) 100%, rgba(255,249,210,1) 100%)",
        }}
      >
        <div className="pt-16 lg:pt-24 flex flex-col gap-10 h-full">
          <div className="pr-1 lg:pr-16">
            <div className="flex justify-end">
              <form
                method="GET"
                onSubmit={handleSearch}
                className="flex shadow-lg rounded-lg"
              >
                <input
                  type="search"
                  name="name"
                  value={searchInput}
                  onChange={(e) => setSearchInput(e.target.value)}
                  className="bg-white px-2 py-1 text-xs lg:text-sm text-slate-800 font-semibold border-2 border-r-0 border-yellow-500 focus:border"
                  placeholder="Cari user..."
                />
                <button
                  type="submit"
                  className="bg-yellow-500 px-2 text-xs lg:text-sm rounded-r-md"
                >
                  <BsSearch className="text-blue-500" />
                </button>
              </form>
            </div>
            {searchParams.get("name") ? (
              <div className="flex justify-end mt-2">
                <button
                  type="button"
                  className="text-white border border-white shadow-lg italic py-1 px-2 rounded-xl"
                  onClick={() => {
                    setSearchInput("");
                    navigate(`/list-users?page=1`);
                    setIsLoading(true);
                    setfetchData(true);
                  }}
                >
                  Hapus pencarian
                </button>
              </div>
            ) : null}
          </div>
          <div className="h-full p-1 flex flex-col justify-between rounded-xl gap-8">
            <div className="">
              <div className="overflow-x-auto shadow-lg rounded-xl">
                <table className="w-full">
                  <thead className="bg-yellow-500 text-slate-800">
                    <tr className="">
                      <th className="p-1 text-sm">No</th>
                      <th className="p-1 text-sm">Nama</th>
                      <th className="p-1 text-sm">Status</th>
                      <th className="p-1 text-sm">Aksi</th>
                    </tr>
                  </thead>
                  <tbody className="bg-white text-center text-slate-800">
                    {users ? (
                      <>
                        {users.map((item, index) => {
                          return (
                            <Itemlist
                              key={index}
                              id={item.id}
                              index={
                                index +
                                1 +
                                8 *
                                  (parseInt(searchParams.get("page") || 1) - 1)
                              }
                              name={item.name}
                              status={
                                item.isOnline ? (
                                  <p className="flex justify-center gap-2 text-green-500 font-semibold">
                                    <span className="self-center">
                                      <IoIosRadioButtonOn />
                                    </span>{" "}
                                    <span>online</span>
                                  </p>
                                ) : (
                                  <p className="italic text-red-500">
                                    Active{" "}
                                    {moment(
                                      momentTz(item.logoutAt)
                                        .tz("Asia/Jakarta")
                                        .format(),
                                      "YYYY-MM-DD h:mm:s"
                                    ).fromNow()}
                                  </p>
                                )
                              }
                            />
                          );
                        })}
                      </>
                    ) : null}
                  </tbody>
                </table>
              </div>
              {users ? (
                <>
                  {users.length === 0 ? (
                    <div className="w-full bg-red-500 text-black rounded-xl py-3 mt-6">
                      <p className="text-center font-semibold flex justify-center gap-2">
                        <span className="self-center">
                          <HiExclamationCircle className="w-[25px] h-[25px] text-red-300" />
                        </span>{" "}
                        <span>
                          Tidak ada data yg cocok dengan "<i>{searchInput}</i>"
                        </span>
                      </p>
                    </div>
                  ) : null}
                </>
              ) : null}
            </div>
            <div className="flex justify-center">
              {pagination ? (
                <div className="btn-group">
                  {pagination?.prev ? (
                    <button
                      type="button"
                      className="bg-transparent border-0 text-blue-500 hover:text-blue-600 italic hover:bg-transparent"
                      onClick={() => {
                        if (searchParams.get("name")) {
                          navigate(
                            `/list-users?name=${searchParams.get(
                              "name"
                            )}&page=${pagination.prev}`
                          );
                        } else {
                          navigate(`/list-users?page=${pagination.prev}`);
                        }
                        setIsLoading(true);
                        setfetchData(true);
                      }}
                    >
                      prev
                    </button>
                  ) : null}

                  <button
                    className="mx-3 text-blue-500 font-bold"
                    type="button"
                  >
                    Page {searchParams.get("page") || "1"}/
                    {pagination?.totals_pages}
                  </button>
                  {pagination?.next ? (
                    <button
                      className="bg-transparent border-0 text-blue-500 hover:text-blue-600 italic hover:bg-transparent"
                      type="button"
                      onClick={() => {
                        if (searchParams.get("name")) {
                          navigate(
                            `/list-users?name=${searchParams.get(
                              "name"
                            )}&page=${pagination.next}`
                          );
                        } else {
                          navigate(`/list-users?page=${pagination.next}`);
                        }
                        setIsLoading(true);
                        setfetchData(true);
                      }}
                    >
                      next
                    </button>
                  ) : null}
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ListUsers;
