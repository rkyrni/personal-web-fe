import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { HiPhone, HiEnvelope, HiMapPin } from "react-icons/hi2";
import moment from "moment";
import Cookies from "js-cookie";
import { reloadInput } from "../../redux/reducer/inputFormCv";

const Template2 = () => {
  const inputFormCv = useSelector((state) => state.inputFormCv.value);
  const dispatch = useDispatch();

  useEffect(() => {
    if (Cookies.get("inputFormCv")) {
      dispatch(reloadInput(JSON.parse(Cookies.get("inputFormCv"))));
    }
  }, []);

  return (
    <div className="relative">
      <div className="min-h-screen p-4">
        <div className="flex flex-col gap-10">
          <div className="h-2/6 flex justify-evenly gap-6">
            <div className="w-2/5 p-2 pl-5">
              <div>
                <figure className="rounded-full p-6 border-l-4 border-cyan-500 relative z-50">
                  <img
                    className="w-[200px] h-[200px] rounded-full z-50 relative"
                    src={Cookies.get("profileResume")}
                    alt="foto"
                  />
                </figure>
              </div>
            </div>
            <div className="w-3/5 flex">
              <div className="self-center pl-6">
                <h2 className="text-5xl text-cyan-500 font-extrabold">
                  {inputFormCv?.name.toUpperCase()}
                </h2>
              </div>
            </div>
          </div>
          {/* bawah */}
          <div className="h-4/6 flex justify-evenly gap-6">
            {/* kiri */}
            <div className="w-2/5 pr-6 flex flex-col gap-10">
              {/* about */}
              {inputFormCv.companyName1 && inputFormCv.position1 ? (
                <div>
                  <p className="text-sm">{inputFormCv?.about}</p>
                </div>
              ) : null}
              {/* kontak */}
              <div>
                <div className="bg-slate-200 p-2 pl-8 mb-6">
                  <h2 className="font-bold text-2xl text-cyan-500">KONTAK</h2>
                </div>
                <div className="flex flex-col gap-3 pl-6">
                  <div className="flex gap-3">
                    <span className="self-center text-cyan-500">
                      <HiPhone />
                    </span>
                    <p>{inputFormCv?.phone}</p>
                  </div>
                  <div className="flex gap-3">
                    <span className="self-center text-cyan-500">
                      <HiEnvelope />
                    </span>
                    <p>{inputFormCv?.email}</p>
                  </div>
                  <div className="flex gap-3">
                    <span className="self-center text-cyan-500">
                      <HiMapPin />
                    </span>
                    <p>{inputFormCv?.address}</p>
                  </div>
                </div>
              </div>
              {/* Skills */}
              <div>
                <div className="bg-slate-200 p-2 pl-8 mb-6">
                  <h2 className="font-bold text-2xl text-cyan-500">
                    KEMAMPUAN
                  </h2>
                </div>
                <div className="flex flex-col gap-2 pl-6">
                  {inputFormCv.skills1 && inputFormCv.skillsPercentage1 ? (
                    <div className="text-black flex justify-between">
                      <p className="self-center">
                        <span className="text-cyan-500 font-extrabold text-xl">
                          -
                        </span>{" "}
                        {inputFormCv?.skills1}
                      </p>
                      <progress
                        className="progress progress-info w-20 self-center bg-slate-600"
                        value={inputFormCv?.skillsPercentage1}
                        max="100"
                      ></progress>
                    </div>
                  ) : null}
                  {inputFormCv.skills2 && inputFormCv.skillsPercentage2 ? (
                    <div className="text-black flex justify-between">
                      <p className="self-center">
                        <span className="text-cyan-500 font-extrabold text-xl">
                          -
                        </span>{" "}
                        {inputFormCv?.skills2}
                      </p>
                      <progress
                        className="progress progress-info w-20 self-center bg-slate-600"
                        value={inputFormCv?.skillsPercentage2}
                        max="100"
                      ></progress>
                    </div>
                  ) : null}
                  {inputFormCv.skills3 && inputFormCv.skillsPercentage3 ? (
                    <div className="text-black flex justify-between">
                      <p className="self-center">
                        <span className="text-cyan-500 font-extrabold text-xl">
                          -
                        </span>{" "}
                        {inputFormCv?.skills3}
                      </p>
                      <progress
                        className="progress progress-info w-20 self-center bg-slate-600"
                        value={inputFormCv?.skillsPercentage3}
                        max="100"
                      ></progress>
                    </div>
                  ) : null}
                  {inputFormCv.skills4 && inputFormCv.skillsPercentage4 ? (
                    <div className="text-black flex justify-between">
                      <p className="self-center">
                        <span className="text-cyan-500 font-extrabold text-xl">
                          -
                        </span>{" "}
                        {inputFormCv?.skills4}
                      </p>
                      <progress
                        className="progress progress-info w-20 self-center bg-slate-600"
                        value={inputFormCv?.skillsPercentage4}
                        max="100"
                      ></progress>
                    </div>
                  ) : null}
                  {inputFormCv.skills5 && inputFormCv.skillsPercentage5 ? (
                    <div className="text-black flex justify-between">
                      <p className="self-center">
                        <span className="text-cyan-500 font-extrabold text-xl">
                          -
                        </span>{" "}
                        {inputFormCv?.skills5}
                      </p>
                      <progress
                        className="progress progress-info w-20 self-center bg-slate-600"
                        value={inputFormCv?.skillsPercentage5}
                        max="100"
                      ></progress>
                    </div>
                  ) : null}
                  {inputFormCv.skills6 && inputFormCv.skillsPercentage6 ? (
                    <div className="text-black flex justify-between">
                      <p className="self-center">
                        <span className="text-cyan-500 font-extrabold text-xl">
                          -
                        </span>{" "}
                        {inputFormCv?.skills6}
                      </p>
                      <progress
                        className="progress progress-info w-20 self-center bg-slate-600"
                        value={inputFormCv?.skillsPercentage6}
                        max="100"
                      ></progress>
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
            {/* kanan */}
            <div className="w-3/5 pr-6 flex flex-col gap-10">
              {/* about */}
              {!inputFormCv.companyName1 && !inputFormCv.position1 ? (
                <div className="">
                  <div className="p-2 bg-cyan-500 pl-8 mb-6">
                    <h2 className="text-slate-200 font-bold text-2xl">
                      DESKRIPSI DIRI
                    </h2>
                  </div>
                  <div className="pl-4">
                    <p>{inputFormCv?.about}</p>
                  </div>
                </div>
              ) : null}
              {/* Pendidikan */}
              <div>
                <div className="p-2 bg-cyan-500 pl-8 mb-6">
                  <h2 className="text-slate-200 font-bold text-2xl">
                    PENDIDIKAN
                  </h2>
                </div>
                <div className="flex flex-col gap-5">
                  {inputFormCv.educationName1 && inputFormCv.jurusan1 ? (
                    <div>
                      <p>
                        {moment(inputFormCv?.fromEducation1).format("YYYY")} -{" "}
                        {moment(inputFormCv?.toEducation1).format("YYYY")}
                      </p>
                      <p className="font-bold text-xl">
                        {inputFormCv?.jurusan1.toUpperCase()}
                      </p>
                      <p>{inputFormCv?.educationName1}</p>
                    </div>
                  ) : null}
                  {inputFormCv.educationName2 && inputFormCv.jurusan2 ? (
                    <div>
                      <p>
                        {moment(inputFormCv?.fromEducation2).format("YYYY")} -{" "}
                        {moment(inputFormCv?.toEducation2).format("YYYY")}
                      </p>
                      <p className="font-bold text-xl">
                        {inputFormCv?.jurusan2.toUpperCase()}
                      </p>
                      <p>{inputFormCv?.educationName2}</p>
                    </div>
                  ) : null}
                  {inputFormCv.educationName3 && inputFormCv.jurusan3 ? (
                    <div>
                      <p>
                        {moment(inputFormCv?.fromEducation3).format("YYYY")} -{" "}
                        {moment(inputFormCv?.toEducation3).format("YYYY")}
                      </p>
                      <p className="font-bold text-xl">
                        {inputFormCv?.jurusan3.toUpperCase()}
                      </p>
                      <p>{inputFormCv?.educationName3}</p>
                    </div>
                  ) : null}
                  {inputFormCv.educationName4 && inputFormCv.jurusan4 ? (
                    <div>
                      <p>
                        {moment(inputFormCv?.fromEducation4).format("YYYY")} -{" "}
                        {moment(inputFormCv?.toEducation4).format("YYYY")}
                      </p>
                      <p className="font-bold text-xl">
                        {inputFormCv?.jurusan4}
                      </p>
                      <p>{inputFormCv?.educationName4}</p>
                    </div>
                  ) : null}
                </div>
              </div>
              {/* Pengalaman */}
              {inputFormCv.companyName1 && inputFormCv.position1 ? (
                <div>
                  <div className="p-2 bg-cyan-500 pl-8 mb-6">
                    <h2 className="text-slate-200 font-bold text-2xl">
                      PENGALAMAN
                    </h2>
                  </div>
                  <div className="flex flex-col gap-5">
                    {inputFormCv.companyName1 && inputFormCv.position1 ? (
                      <div>
                        <p>
                          {moment(inputFormCv?.fromWorkExperience1).format(
                            "YYYY"
                          )}{" "}
                          -{" "}
                          {moment(inputFormCv?.toWorkExperience1).format(
                            "YYYY"
                          )}
                        </p>
                        <p className="font-bold text-xl">
                          {inputFormCv?.position1.toUpperCase()}
                        </p>
                        <p>{inputFormCv?.companyName1}</p>
                      </div>
                    ) : null}
                    {inputFormCv.companyName2 && inputFormCv.position2 ? (
                      <div>
                        <p>
                          {moment(inputFormCv?.fromWorkExperience2).format(
                            "YYYY"
                          )}{" "}
                          -{" "}
                          {moment(inputFormCv?.toWorkExperience2).format(
                            "YYYY"
                          )}
                        </p>
                        <p className="font-bold text-xl">
                          {inputFormCv?.position2.toUpperCase()}
                        </p>
                        <p>{inputFormCv?.companyName2}</p>
                      </div>
                    ) : null}
                    {inputFormCv.companyName3 && inputFormCv.position3 ? (
                      <div>
                        <p>
                          {moment(inputFormCv?.fromWorkExperience3).format(
                            "YYYY"
                          )}{" "}
                          -{" "}
                          {moment(inputFormCv?.toWorkExperience3).format(
                            "YYYY"
                          )}
                        </p>
                        <p className="font-bold text-xl">
                          {inputFormCv?.position3.toUpperCase()}
                        </p>
                        <p>{inputFormCv?.companyName3}</p>
                      </div>
                    ) : null}
                  </div>
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </div>
      {/* hiasan */}
      <div className="w-[100px] h-[100px] absolute bg-cyan-200 rounded-full shadow-lg top-0 left-0"></div>
      <div className="w-[100px] h-[100px] absolute bg-cyan-200 bottom-0 right-0"></div>
    </div>
  );
};

export default Template2;
