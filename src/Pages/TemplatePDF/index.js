import Cookies from "js-cookie";
import React, { useRef } from "react";
import { HiArrowDownTray } from "react-icons/hi2";
import { useNavigate } from "react-router-dom";
import { useReactToPrint } from "react-to-print";
import Template1 from "./Template1";
import Template2 from "./Template2";

const TemplatePDF = () => {
  const navigate = useNavigate();
  const componentRef = useRef();

  const handlePdf = useReactToPrint({
    content: () => componentRef.current,
    documentTitle: Cookies.get("inputForm") || "nama",
  });

  const Template = () => {
    if (Cookies.get("template") === "TEMPLATE-1") return <Template2 />;
    else if (Cookies.get("template") === "TEMPLATE-2") return <Template1 />;
  };

  return (
    <>
      <div
        ref={componentRef}
        style={{ width: "100%", minHeight: "100vh" }}
        className="bg-white text-black"
      >
        <Template />
      </div>
      <div className="absolute top-10 lg:left-1/3 gap-5 z-30 flex flex-col lg:flex-row w-full">
        <button
          type="button"
          onClick={() => navigate("/input-form")}
          className="self-center text-black bg-red-500 py-2 px-4 rounded"
        >
          Kembali ke form
        </button>
        <button
          type="button"
          className="self-center text-black bg-green-500 py-2 px-4 rounded"
          onClick={() => navigate("/pilih-template")}
        >
          Ganti template
        </button>
        <button
          type="button"
          className="bg-slate-400 self-center py-2 px-4 rounded text-black flex"
          onClick={handlePdf}
        >
          <span>Download</span>{" "}
          <span className="self-center">
            <HiArrowDownTray />
          </span>
        </button>
      </div>
    </>
  );
};

export default TemplatePDF;
