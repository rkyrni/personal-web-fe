import Cookies from "js-cookie";
import moment from "moment/moment";
import React, { useEffect } from "react";
import { HiPhone, HiEnvelope, HiMapPin } from "react-icons/hi2";
import { useDispatch, useSelector } from "react-redux";
import { reloadInput } from "../../redux/reducer/inputFormCv";

const Template1 = () => {
  const inputFormCv = useSelector((state) => state.inputFormCv.value);
  const dispatch = useDispatch();

  useEffect(() => {
    if (Cookies.get("inputFormCv")) {
      dispatch(reloadInput(JSON.parse(Cookies.get("inputFormCv"))));
    }
  }, []);

  return (
    <div className="relative">
      <div className="px-5 py-8">
        <div className="flex gap-7">
          {/* Sisi Kiri */}
          <div className="w-2/5">
            <div className="flex flex-col gap-5">
              {/* Foto */}
              <div className="self-center z-10">
                <figure>
                  <img
                    className="w-[200px] h-[270px] border-2 border-yellow-500"
                    src={Cookies.get("profileResume")}
                    alt="profileImage"
                  />
                </figure>
              </div>
              {/* Name */}
              <div className="mb-8">
                <div>
                  <h2 className="text-5xl font-extrabold text-center text-black drop-shadow-lg shadow-yellow-500">
                    {inputFormCv?.name}
                  </h2>
                </div>
              </div>
              {/* Kontak */}
              <div className="pl-6 pr-3">
                <div className="border-b-2 border-yellow-500 mb-4 pb-2">
                  <h2 className="text-black font-bold text-2xl">KONTAK</h2>
                </div>
                <div className="pl-3 flex flex-col gap-2">
                  <div className="flex gap-3">
                    <span className="self-center text-yellow-500">
                      <HiPhone />
                    </span>
                    <p>{inputFormCv?.phone}</p>
                  </div>
                  <div className="flex gap-3">
                    <span className="self-center text-yellow-500">
                      <HiEnvelope />
                    </span>
                    <p>{inputFormCv?.email}</p>
                  </div>
                  <div className="flex gap-3">
                    <span className="self-center text-yellow-500">
                      <HiMapPin />
                    </span>
                    <p>{inputFormCv?.address}</p>
                  </div>
                </div>
              </div>
              {/* Kemampuan */}
              <div className="pl-6 pr-3">
                <div className="border-b-2 border-yellow-500 mb-4 pb-2">
                  <h2 className="text-black font-bold text-2xl">KEMAMPUAN</h2>
                </div>
                <div className="">
                  {inputFormCv.skills1 && inputFormCv.skillsPercentage1 ? (
                    <div className="text-black flex justify-between">
                      <p className="self-center">
                        <span className="text-yellow-500 font-extrabold text-xl">
                          -
                        </span>{" "}
                        {inputFormCv?.skills1}
                      </p>
                      <progress
                        className="progress progress-warning w-20 self-center bg-slate-600"
                        value={inputFormCv?.skillsPercentage1}
                        max="100"
                      ></progress>
                    </div>
                  ) : null}
                  {inputFormCv.skills2 && inputFormCv.skillsPercentage2 ? (
                    <div className="text-black flex justify-between">
                      <p className="self-center">
                        <span className="text-yellow-500 font-extrabold text-xl">
                          -
                        </span>{" "}
                        {inputFormCv?.skills2}
                      </p>
                      <progress
                        className="progress progress-warning w-20 self-center bg-slate-600"
                        value={inputFormCv?.skillsPercentage2}
                        max="100"
                      ></progress>
                    </div>
                  ) : null}
                  {inputFormCv.skills3 && inputFormCv.skillsPercentage3 ? (
                    <div className="text-black flex justify-between">
                      <p className="self-center">
                        <span className="text-yellow-500 font-extrabold text-xl">
                          -
                        </span>{" "}
                        {inputFormCv?.skills3}
                      </p>
                      <progress
                        className="progress progress-warning w-20 self-center bg-slate-600"
                        value={inputFormCv?.skillsPercentage3}
                        max="100"
                      ></progress>
                    </div>
                  ) : null}
                  {inputFormCv.skills4 && inputFormCv.skillsPercentage4 ? (
                    <div className="text-black flex justify-between">
                      <p className="self-center">
                        <span className="text-yellow-500 font-extrabold text-xl">
                          -
                        </span>{" "}
                        {inputFormCv?.skills4}
                      </p>
                      <progress
                        className="progress progress-warning w-20 self-center bg-slate-600"
                        value={inputFormCv?.skillsPercentage4}
                        max="100"
                      ></progress>
                    </div>
                  ) : null}
                  {inputFormCv.skills5 && inputFormCv.skillsPercentage5 ? (
                    <div className="text-black flex justify-between">
                      <p className="self-center">
                        <span className="text-yellow-500 font-extrabold text-xl">
                          -
                        </span>{" "}
                        {inputFormCv?.skills5}
                      </p>
                      <progress
                        className="progress progress-warning w-20 self-center bg-slate-600"
                        value={inputFormCv?.skillsPercentage5}
                        max="100"
                      ></progress>
                    </div>
                  ) : null}
                  {inputFormCv.skills6 && inputFormCv.skillsPercentage6 ? (
                    <div className="text-black flex justify-between">
                      <p className="self-center">
                        <span className="text-yellow-500 font-extrabold text-xl">
                          -
                        </span>{" "}
                        {inputFormCv?.skills6}
                      </p>
                      <progress
                        className="progress progress-warning w-20 self-center bg-slate-600"
                        value={inputFormCv?.skillsPercentage6}
                        max="100"
                      ></progress>
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
          {/* Sisi Kanan */}
          <div className="w-3/5 flex relative">
            <div
              className={`w-full flex flex-col ${
                inputFormCv.companyName1 ? "justify-center" : "justify-end"
              } gap-8`}
            >
              {/* Deskripsi Diri */}
              <div>
                <div className="border-b-2 border-yellow-500 mb-4 pb-2">
                  <h2 className="text-black font-bold text-2xl">
                    DESKRIPSI DIRI
                  </h2>
                </div>
                <div>
                  <p>{inputFormCv?.about}</p>
                </div>
              </div>
              {/* Pendidikan */}
              <div>
                <div className="border-b-2 border-yellow-500 mb-4 pb-2">
                  <h2 className="text-black font-bold text-2xl">PENDIDIKAN</h2>
                </div>
                <div className="flex flex-col gap-5">
                  {inputFormCv.educationName1 && inputFormCv.jurusan1 ? (
                    <div>
                      <p>
                        {moment(inputFormCv?.fromEducation1).format("YYYY")} -{" "}
                        {moment(inputFormCv?.toEducation1).format("YYYY")}
                      </p>
                      <p className="font-bold text-xl">
                        {inputFormCv?.jurusan1.toUpperCase()}
                      </p>
                      <p>{inputFormCv?.educationName1}</p>
                    </div>
                  ) : null}
                  {inputFormCv.educationName2 && inputFormCv.jurusan2 ? (
                    <div>
                      <p>
                        {moment(inputFormCv?.fromEducation2).format("YYYY")} -{" "}
                        {moment(inputFormCv?.toEducation2).format("YYYY")}
                      </p>
                      <p className="font-bold text-xl">
                        {inputFormCv?.jurusan2.toUpperCase()}
                      </p>
                      <p>{inputFormCv?.educationName2}</p>
                    </div>
                  ) : null}
                  {inputFormCv.educationName3 && inputFormCv.jurusan3 ? (
                    <div>
                      <p>
                        {moment(inputFormCv?.fromEducation3).format("YYYY")} -{" "}
                        {moment(inputFormCv?.toEducation3).format("YYYY")}
                      </p>
                      <p className="font-bold text-xl">
                        {inputFormCv?.jurusan3.toUpperCase()}
                      </p>
                      <p>{inputFormCv?.educationName3}</p>
                    </div>
                  ) : null}
                  {inputFormCv.educationName4 && inputFormCv.jurusan4 ? (
                    <div>
                      <p>
                        {moment(inputFormCv?.fromEducation4).format("YYYY")} -{" "}
                        {moment(inputFormCv?.toEducation4).format("YYYY")}
                      </p>
                      <p className="font-bold text-xl">
                        {inputFormCv?.jurusan4}
                      </p>
                      <p>{inputFormCv?.educationName4}</p>
                    </div>
                  ) : null}
                </div>
              </div>
              {/* Pengalaman */}
              {inputFormCv.companyName1 && inputFormCv.position1 ? (
                <div>
                  <div className="border-b-2 border-yellow-500 mb-4 pb-2">
                    <h2 className="text-black font-bold text-2xl">
                      PENGALAMAN KERJA
                    </h2>
                  </div>
                  <div className="flex flex-col gap-5">
                    {inputFormCv.companyName1 && inputFormCv.position1 ? (
                      <div>
                        <p>
                          {moment(inputFormCv?.fromWorkExperience1).format(
                            "YYYY"
                          )}{" "}
                          -{" "}
                          {moment(inputFormCv?.toWorkExperience1).format(
                            "YYYY"
                          )}
                        </p>
                        <p className="font-bold text-xl">
                          {inputFormCv?.position1.toUpperCase()}
                        </p>
                        <p>{inputFormCv?.companyName1}</p>
                      </div>
                    ) : null}
                    {inputFormCv.companyName2 && inputFormCv.position2 ? (
                      <div>
                        <p>
                          {moment(inputFormCv?.fromWorkExperience2).format(
                            "YYYY"
                          )}{" "}
                          -{" "}
                          {moment(inputFormCv?.toWorkExperience2).format(
                            "YYYY"
                          )}
                        </p>
                        <p className="font-bold text-xl">
                          {inputFormCv?.position2.toUpperCase()}
                        </p>
                        <p>{inputFormCv?.companyName2}</p>
                      </div>
                    ) : null}
                    {inputFormCv.companyName3 && inputFormCv.position3 ? (
                      <div>
                        <p>
                          {moment(inputFormCv?.fromWorkExperience3).format(
                            "YYYY"
                          )}{" "}
                          -{" "}
                          {moment(inputFormCv?.toWorkExperience3).format(
                            "YYYY"
                          )}
                        </p>
                        <p className="font-bold text-xl">
                          {inputFormCv?.position3.toUpperCase()}
                        </p>
                        <p>{inputFormCv?.companyName3}</p>
                      </div>
                    ) : null}
                  </div>
                </div>
              ) : null}
            </div>
            {/* hiasan */}
            <div className="bg-yellow-500 absolute w-full h-5 bottom-0 -mb-[120px]"></div>
          </div>
        </div>
        {/* Hiasan */}
      </div>
      <div className="min-w-full h-48 bg-slate-400 absolute top-0 opacity-20"></div>
      <div className="h-48 w-24 bg-yellow-500 absolute top-0"></div>
    </div>
  );
};

export default Template1;
