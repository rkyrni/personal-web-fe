import React from "react";
import { useInView } from "react-intersection-observer";
import Certificate1 from "../../assets/img/cr-1.png";
import Certificate2 from "../../assets/img/cr-2.png";
import Certificate3 from "../../assets/img/cr-3.png";
import Certificate4 from "../../assets/img/cr-4.png";
import Certificate5 from "../../assets/img/cr-5.png";

const Certificate = () => {
  const { ref: certificateRef, inView: certificateIsVisible } = useInView();

  const ItemList = ({ text, image, tittle, link }) => {
    return (
      <div className={`flex flex-col transition duration-1000`}>
        <a href={link} className="self-center max-w-max">
          <img
            className="w-[150px] h-[150px] hover:scale-y-75 hover:scale-x-125 transition duration-300"
            src={image}
            alt="certificate"
          />
        </a>
        <p className="text-center text-slate-100">{tittle}</p>
        <p className="text-center text-slate-100">{text}</p>
      </div>
    );
  };

  return (
    <div ref={certificateRef} className="bg-[#00ADB5] py-20 overflow-x-hidden">
      <h2
        className={`${
          certificateIsVisible
            ? "translate-x-0 opacity-100"
            : "-translate-x-[100px] opacity-0"
        } text-4xl text-center font-bold mb-10 text-slate-200 transition duration-1000`}
      >
        Certificates
      </h2>
      <div>
        <div
          className={`${
            certificateIsVisible
              ? "translate-x-0 opacity-100"
              : "translate-x-[100px] opacity-0"
          } flex justify-center gap-8 flex-wrap transition duration-1000`}
        >
          <ItemList
            text={"Graduated from Binar Academy"}
            image={Certificate1}
            tittle="Full-Stack Web"
            link={
              "https://drive.google.com/file/d/1lgQylQvov6Rl5mRCV9oTgfjWnV3GogW6/view?usp=share_link"
            }
          />
          <ItemList
            text={"The Most Proggresive Student"}
            image={Certificate2}
            tittle="Full-Stack Web"
            link="https://drive.google.com/file/d/1x9y4bmnLE3hrxQW5J9opTBXmi_1mchUo/view?usp=share_link"
          />
          <ItemList
            text={"Graduated from Sanbercode"}
            image={Certificate3}
            tittle="React Js Web Frontend"
            link={
              "https://drive.google.com/file/d/14YrYGA8AAUh_ehosJJh3botwIYe-Pi4N/view?usp=share_link"
            }
          />
          <ItemList
            text={"HackerRank Coding test"}
            image={Certificate4}
            tittle="React Js Basic"
            link={
              "https://drive.google.com/file/d/19EJUK8OVhL5cD-M-xO8EN7DQzA3rHTTM/view?usp=share_link"
            }
          />
          <ItemList
            text={"HackerRank Coding test"}
            image={Certificate5}
            tittle="Javascript Basic"
            link={
              "https://drive.google.com/file/d/1SjQzQ10rFAfP2KWI5w7fhMB793kci9ob/view?usp=share_link"
            }
          />
        </div>
      </div>
    </div>
  );
};

export default Certificate;
