import React, { useState } from "react";
import { useInView } from "react-intersection-observer";
import PicPortfolio1 from "../../assets/img/pf-1.png";
import PicPortfolio2 from "../../assets/img/pf-2.png";
import PicPortfolio3 from "../../assets/img/pf-3.png";
import PicPortfolio4 from "../../assets/img/pf-4.png";
import PicPortfolio5 from "../../assets/img/pf-5.jfif";
import background from "../../assets/img/bg-portfolio.jpg";

const Portfolios = ({ themeColor }) => {
  const { ref: portfoliosRef, inView: portfoliosIsVisible } = useInView();

  const ItemsProject = ({ tittle, image, link, tooltip }) => {
    const [showTooltip, setShowTooltip] = useState(false);
    return (
      <section className="w-64">
        <div
          className="w-56 h-56 flex flex-col justify-center relative rounded-lg shadow-lg hover-shadow transition duration-300 hover:-translate-x-1 hover:-translate-y-3 py-5 mb-1"
          style={{ background: themeColor }}
        >
          <div
            className={`${
              showTooltip ? "tooltip-open" : ""
            } tooltip tooltip-bottom lg:tooltip-left tooltip-info absolute top-2 right-20 lg:right-2`}
            data-tip={tooltip}
          >
            <button
              onClick={() => setShowTooltip(!showTooltip)}
              className={`${
                showTooltip ? "bg-slate-500" : "bg-slate-300"
              } w-6 h-6 rounded-full text-black hover:shadow-lg hover:bg-slate-500 font-bold`}
            >
              !
            </button>
          </div>
          <p className="font-bold text-center text-white mt-5">{tittle}</p>
          <img src={image} alt="pf-1" className="w-52 h-28 self-center mb-5" />
          <div className="bg-black w-full flex justify-end pr-7 ml-5">
            <a href={link} className="text-blue-500 italic text-end">
              try on...
            </a>
          </div>
        </div>
        {/* <div className="px-1">
          <p className="font-semibold">WEB</p>
          <p className="font-bold">{tittle}</p>
        </div> */}
      </section>
    );
  };

  return (
    <div className="min-h-[70vh] mb-20">
      <div
        style={{ backgroundImage: `url(${background})` }}
        ref={portfoliosRef}
        className="bg-center bg-cover text-slate-800 py-3 shadow-lg relative overflow-x-hidden"
      >
        <div
          className={`${
            portfoliosIsVisible ? "-translate-x-[1400px]" : "translate-x-0"
          } absolute top-0 bottom-0 left-0 right-0 rounded-xl z-50 transition duration-1000 bg-black`}
          // style={{
          //   background:
          //     "linear-gradient(270deg, rgba(173,141,97,1) 0%, rgba(159,88,88,1) 52%)",
          // }}
        ></div>
        <div className="text-center mb-10">
          <h3 className="lg:text-4xl text-2xl text-white drop-shadow-xl font-bold">
            Fortfolios
          </h3>
        </div>
        <div className="lg:px-36">
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-3 justify-items-center gap-8">
            <ItemsProject
              tittle={"Mini Games"}
              image={PicPortfolio1}
              tooltip={
                "simple game play that has implemented game scores and player best scores, and there is a leaderboard for each game"
              }
              link="https://rickyricky-web.netlify.app/#/game-list"
            />
            <ItemsProject
              tittle={"Bot Whatsapp"}
              image={PicPortfolio5}
              tooltip={
                "A whatsapp bot that has several features such as chatGPT and changing the background on photos"
              }
              link="https://wa.link/vbsmxm"
            />
            <ItemsProject
              tittle={"Create Resume"}
              image={PicPortfolio2}
              tooltip={
                "create a resume quickly and with a selection of templates that are already available"
              }
              link="https://rickyricky-web.netlify.app/#/input-form"
            />
            <ItemsProject
              tittle={"Realtime Chat"}
              image={PicPortfolio3}
              tooltip={
                "realtime chat that has implemented a realtime database, can send text messages, pictures and videos as well"
              }
              link="https://tim1-fsw24-binar.netlify.app/"
            />
            <ItemsProject
              tittle={"CRUD (to do list)"}
              image={PicPortfolio4}
              tooltip={
                "Processing student data with the CRUD system, there are also pagination and search features. If you want to login, use Login ID: 1234 or 4321"
              }
              link="https://ricky-ramadani-binar-mock-test.netlify.app/"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Portfolios;
