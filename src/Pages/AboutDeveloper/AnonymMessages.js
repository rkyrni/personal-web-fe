import axios from "axios";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";

const AnonymMessages = ({ isLoading, setIsLoading }) => {
  const [data, setData] = useState(null);
  const [fetchData, setFecthData] = useState(true);
  const [input, setInput] = useState({
    tittle: "",
    text: "",
  });

  useEffect(() => {
    const fetch = async () => {
      setIsLoading(true);
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_BASE_URL}/anonym-messages`
        );

        setData(response.data.data);
        setIsLoading(false);
      } catch (error) {
        alert(error);
        setIsLoading(false);
      }
    };

    if (fetchData) {
      fetch();
      setFecthData(false);
    }
  }, [fetchData]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    setIsLoading(true);
    try {
      const response = await axios.post(
        `${process.env.REACT_APP_BASE_URL}/anonym-messages`,
        input
      );

      setData(response.data.data);
      setFecthData(true);
      setInput({
        tittle: "",
        text: "",
      });
      setIsLoading(false);
    } catch (error) {
      alert(error);
      setIsLoading(false);
    }
  };

  const handleChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  return (
    <div className="flex flex-col lg:pt-5 gap-6">
      <form
        method="POST"
        onSubmit={handleSubmit}
        className="flex flex-col gap-2"
      >
        <div className="self-center">
          <input
            type="text"
            name="tittle"
            value={input.tittle}
            onChange={handleChange}
            required
            maxLength={20}
            placeholder="Tittle"
            className="input-animation bg-white text-slate-800 px-2 py-1 font-bold text-xs lg:text-base"
          />
        </div>
        <div className="self-center">
          <textarea
            className="bg-white text-slate-800 px-2 py-1 text-xs lg:text-base"
            name="text"
            value={input.text}
            onChange={handleChange}
            maxLength={90}
            id=""
            cols="30"
            rows="3"
            placeholder="Text"
          ></textarea>
        </div>
        <div className="self-center px-2 py-1 bg-green-500 rounded-lg font-bold">
          <button type="submit">send</button>
        </div>
      </form>
      <div>
        <div>
          <div className="flex flex-wrap gap-3 overflow-y-auto max-h-96 bg-white rounded-xl p-1 scrool-bar">
            {data !== null &&
              data.map((item, i) => {
                return (
                  <div
                    key={i}
                    className={`bg-[#EAFDFC] rounded-lg shadow-lg p-2 max-w-lg self-baseline max-h-max text-xs lg:text-base`}
                  >
                    <p className="text-center">
                      <strong>{item.tittle}</strong>
                    </p>
                    <p>{item.text}</p>
                  </div>
                );
              })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default AnonymMessages;
