import React, { useState } from "react";

const NavbarAbout = ({ setThemeColor, themeColor }) => {
  const [navigationTheme, setNavigationTheme] = useState(false);
  const bgMojito =
    "linear-gradient(175deg, rgba(130,156,165,1) 9%, rgba(128,128,128,1) 43%, rgba(139,139,139,1) 56%, rgba(241,241,241,1) 100%)";
  const bgLime =
    "linear-gradient(270deg, rgba(173,171,97,1) 0%, rgba(134,159,88,1) 52%)";
  const bgButterfly =
    "linear-gradient(270deg, rgba(185,185,185,1) 0%, rgba(88,141,159,1) 47%)";
  const bgMalibu =
    "linear-gradient(172deg, rgba(16,60,231,1) 0%, rgba(100,233,255,1) 41%)";
  const bgPeach =
    "linear-gradient(172deg, rgba(255,132,115,1) 0%, rgba(255,249,210,1) 33%)";

  return (
    <div
      className="fixed top-0 right-0 left-0"
      style={{
        backgroundColor: "rgba(0, 0, 0, 0.4)",
        zIndex: 100,
        maxHeight: "50px",
      }}
    >
      <div style={{ backdropFilter: "blur(5px)", maxHeight: "50px" }}>
        <div className="flex justify-around py-3">
          <div>
            <p className="text-2xl font-bold text-[#00ADB5]">Ricky Ramadani</p>
          </div>
          <div className="flex gap-3">
            <div
              className={`${
                navigationTheme ? "-mt-16" : "-mt-20"
              } flex relative justify-center w-[170px] h-[170px]`}
            >
              <div
                onClick={() => setNavigationTheme(true)}
                className="self-center relative w-[40px] h-[40px] cursor-pointer flex justify-center duration-500"
              >
                <div
                  onClick={() => {
                    setThemeColor(bgMojito);
                  }}
                  style={{
                    transform: `translate(calc(${
                      navigationTheme ? "60" : "14"
                    }px*-1),calc(${navigationTheme ? "60" : "14"}px*0))`,
                    transitionDelay: "calc(0.1s*0)",
                    background: `${navigationTheme ? bgMojito : "#FFF"}`,
                    width: `${navigationTheme ? "45px" : "7px"}`,
                    height: `${navigationTheme ? "45px" : "7px"}`,
                  }}
                  className={`${
                    navigationTheme ? "w-[35px] h-[35px] rounded-full" : "w-0"
                  } absolute self-center flex justify-center transition-all duration-500
                   ${themeColor === bgMojito ? "border-4 border-white" : ""}`}
                >
                  <span
                    style={{
                      fontSize: `${navigationTheme ? "1.35em" : "0em"}`,
                    }}
                    className={`self-center text-white duration-500`}
                  ></span>
                </div>
                <div
                  onClick={() => setThemeColor(bgMalibu)}
                  style={{
                    transform: `translate(calc(${
                      navigationTheme ? "60" : "14"
                    }px*1),calc(${navigationTheme ? "60" : "14"}px*0))`,
                    transitionDelay: "calc(0.1s*1)",
                    background: `${navigationTheme ? bgMalibu : "#FFF"}`,
                    width: `${navigationTheme ? "45px" : "7px"}`,
                    height: `${navigationTheme ? "45px" : "7px"}`,
                  }}
                  className={`${
                    navigationTheme ? "w-[35px] h-[35px] rounded-full" : "w-0"
                  } absolute self-center flex justify-center transition-all duration-500
                  ${themeColor === bgMalibu ? "border-4 border-white" : ""}`}
                >
                  <span
                    style={{
                      fontSize: `${navigationTheme ? "1.35em" : "0em"}`,
                    }}
                    className="self-center text-white duration-500"
                  ></span>
                </div>
                {/* <p
                  style={{
                    transform: `translate(calc(${
                      navigationTheme ? "60" : "14"
                    }px*0),calc(${navigationTheme ? "60" : "14"}px*-1))`,
                    transitionDelay: "calc(0.1s*2)",
                    backgroundColor: `${navigationTheme ? "#1F2536" : "#FFF"}`,
                    width: `${navigationTheme ? "45px" : "7px"}`,
                  }}
                  className={`${
                    navigationTheme ? "w-[35px] h-[35px] rounded-full" : "w-0"
                  } absolute self-center flex justify-center transition-all duration-500`}
                >
                  <span
                    style={{
                      fontSize: `${navigationTheme ? "1.35em" : "0em"}`,
                    }}
                    className="self-center text-white duration-500"
                  >
                    3
                  </span>
                </p> */}
                <div
                  onClick={() => setThemeColor(bgLime)}
                  style={{
                    transform: `translate(calc(${
                      navigationTheme ? "60" : "14"
                    }px*0),calc(${navigationTheme ? "60" : "14"}px*1))`,
                    transitionDelay: "calc(0.1s*3)",
                    background: `${navigationTheme ? bgLime : "#FFF"}`,
                    width: `${navigationTheme ? "45px" : "7px"}`,
                    height: `${navigationTheme ? "45px" : "7px"}`,
                  }}
                  className={`${
                    navigationTheme ? "w-[35px] h-[35px] rounded-full" : "w-0"
                  } absolute self-center flex justify-center transition-all duration-500
                  ${themeColor === bgLime ? "border-4 border-white" : ""}`}
                >
                  <span
                    style={{
                      fontSize: `${navigationTheme ? "1.35em" : "0em"}`,
                    }}
                    className="self-center text-white duration-500"
                  ></span>
                </div>
                <div
                  onClick={() => setThemeColor(bgButterfly)}
                  style={{
                    transform: `translate(calc(${
                      navigationTheme ? "60" : "14"
                    }px*-1),calc(${navigationTheme ? "60" : "14"}px*1))`,
                    transitionDelay: "calc(0.1s*4)",
                    background: `${navigationTheme ? bgButterfly : "#FFF"}`,
                    width: `${navigationTheme ? "45px" : "7px"}`,
                    height: `${navigationTheme ? "45px" : "7px"}`,
                  }}
                  className={`${
                    navigationTheme ? "w-[35px] h-[35px] rounded-full" : "w-0"
                  } absolute self-center flex justify-center transition-all duration-500 ${
                    themeColor === bgButterfly ? "border-4 border-white" : ""
                  }`}
                >
                  <span
                    style={{
                      fontSize: `${navigationTheme ? "1.35em" : "0em"}`,
                    }}
                    className="self-center text-white duration-500"
                  ></span>
                </div>
                {/* <p
                  style={{
                    transform: `translate(calc(${
                      navigationTheme ? "60" : "14"
                    }px*-1),calc(${navigationTheme ? "60" : "14"}px*-1))`,
                    transitionDelay: "calc(0.1s*5)",
                    backgroundColor: `${navigationTheme ? "#1F2536" : "#FFF"}`,
                    width: `${navigationTheme ? "45px" : "7px"}`,
                  }}
                  className={`${
                    navigationTheme ? "w-[35px] h-[35px] rounded-full" : "w-0"
                  } absolute self-center flex justify-center transition-all duration-500`}
                >
                  <span
                    style={{
                      fontSize: `${navigationTheme ? "1.35em" : "0em"}`,
                    }}
                    className="self-center text-white duration-500"
                  >
                    6
                  </span>
                </p> */}
                {/* <p
                  style={{
                    transform: `translate(calc(${
                      navigationTheme ? "60" : "14"
                    }px*1),calc(${navigationTheme ? "60" : "14"}px*-1))`,
                    transitionDelay: "calc(0.1s*6)",
                    backgroundColor: `${navigationTheme ? "#1F2536" : "#FFF"}`,
                    width: `${navigationTheme ? "45px" : "7px"}`,
                  }}
                  className={`${
                    navigationTheme ? "w-[35px] h-[35px] rounded-full" : "w-0"
                  } absolute self-center flex justify-center transition-all duration-500`}
                >
                  <span
                    style={{
                      fontSize: `${navigationTheme ? "1.35em" : "0em"}`,
                    }}
                    className="self-center text-white duration-500"
                  >
                    7
                  </span>
                </p> */}
                <div
                  onClick={() => setThemeColor(bgPeach)}
                  style={{
                    transform: `translate(calc(${
                      navigationTheme ? "60" : "14"
                    }px*1),calc(${navigationTheme ? "60" : "14"}px*1))`,
                    transitionDelay: "calc(0.1s*7)",
                    background: `${navigationTheme ? bgPeach : "#FFF"}`,
                    width: `${navigationTheme ? "45px" : "7px"}`,
                    height: `${navigationTheme ? "45px" : "7px"}`,
                  }}
                  className={`${
                    navigationTheme ? "w-[35px] h-[35px] rounded-full" : "w-0"
                  } absolute self-center flex justify-center transition-all duration-500
                  ${themeColor === bgPeach ? "border-4 border-white" : ""}`}
                >
                  <span
                    style={{
                      fontSize: `${navigationTheme ? "1.35em" : "0em"}`,
                    }}
                    className="self-center text-white duration-500"
                  ></span>
                </div>
              </div>
              <div
                onClick={() => setNavigationTheme(false)}
                className={`${
                  navigationTheme
                    ? "w-[40px] h-[40px] duration-700 bg-[#2DFC52] hover:bg-green-600 pointer-events-auto"
                    : "w-[7px] h-[7px] bg-white pointer-events-none duration-500"
                } delay-300 absolute flex justify-center self-center cursor-pointer`}
              >
                <span
                  className={`${
                    navigationTheme
                      ? "scale-100 delay-1000 text-[#030304]"
                      : "text-xl scale-0 text-[#030304] duration-500"
                  } self-center font-bold`}
                >
                  X
                </span>
              </div>
            </div>
            {/* disini kalo mau tambah menu */}
            {/* <p>
                menu
            </p>
            <p>
                menu
            </p> */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default NavbarAbout;
