import React, { useRef, useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  IoLogoWhatsapp,
  IoLogoInstagram,
  IoLogoGithub,
  IoIosHome,
  IoLogoLinkedin,
} from "react-icons/io";
import { FaGitlab, FaFacebook } from "react-icons/fa";
import { HiArrowDown, HiPencilSquare, HiStar } from "react-icons/hi2";
import Hand from "../../assets/img/hand.gif";
import NavbarAbout from "./NavbarAbout";
import FooterAbout from "./FooterAbout";
import Skills from "./Skills";
import Portfolios from "./Portfolios";
import AboutMe from "./AboutMe";
import ButtonOpsi from "./ButtonOpsi";
import ModalAbout from "./ModalAbout";
import Steps from "./Steps";
import { useInView } from "react-intersection-observer";
import AnonymMessages from "./AnonymMessages";
import Loading from "../../Components/Loading";
import Certificate from "./Certificate";

const AboutDeveloper = () => {
  const [showModalSteps, setShowModalSteps] = useState(false);
  const [showModalAnonym, setShowModalAnonym] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [themeColor, setThemeColor] = useState(
    "linear-gradient(270deg, rgba(185,185,185,1) 0%, rgba(88,141,159,1) 47%)"
  );

  const { ref: section1ref, inView: section1IsVisible } = useInView();

  const textProfesi = "Software Developer";
  const themeMode = useSelector((state) => state.themeMode.value);
  const headRef = useRef(null);
  const projectRef = useRef(null);
  const aboutMeRef = useRef(null);

  const goto = (ref) => {
    window.scrollTo({
      top: ref.offsetTop,
      left: 0,
      behavior: "smooth",
    });
  };

  const ItemBiodatas = ({ text, icon, href, entry }) => {
    return (
      <a
        href={href}
        rel="noreferrer"
        className={`bg-[#EAFDFC] relative py-2 rounded-3xl w-full flex justify-center border border-black lg:self-center hover:bg-slate-200 transition duration-[2000ms]`}
        style={{ boxShadow: "7px 11px 2px -2px rgba(0,0,0,0.7)" }}
      >
        <span className="absolute left-2 top-1">{icon}</span>
        <span className="text-slate-800 text-xs font-semibold">{text}</span>
      </a>
    );
  };

  const TextAnimation = ({ char }) => {
    return (
      <span
        className="text-shadow-custom hover:text-red-500 inline-block hover:animate-bounce ease-in-out duration-300"
        style={{ animationIterationCount: 2 }}
      >
        {char}
      </span>
    );
  };

  return (
    <div>
      <div
        className={`${
          themeMode ? "bg-black text-slate-200" : "bg-slate-200 text-slate-800"
        } min-h-screen`}
        ref={headRef}
      >
        {isLoading ? <Loading /> : null}
        <div
          className={`rounded-lg flex flex-col gap-5 relative`}
          style={{
            background: themeColor,
          }}
        >
          {showModalAnonym ? (
            <ModalAbout
              tittle={"Anonym Messages"}
              setIsModalView={setShowModalAnonym}
              body={<AnonymMessages setIsLoading={setIsLoading} />}
            />
          ) : null}
          {showModalSteps ? (
            <ModalAbout
              setIsModalView={setShowModalSteps}
              tittle={"Professional Experiences"}
              body={<Steps />}
            />
          ) : null}

          <ButtonOpsi
            setButton_1_show={setShowModalSteps}
            setButton_2_show={setShowModalAnonym}
          />
          <NavbarAbout setThemeColor={setThemeColor} themeColor={themeColor} />
          <div>
            <a
              href="https://www.instagram.com/ricky_rd05/"
              className="absolute right-5 lg:right-80 top-20 rounded-xl px-3 text-slate-800 text-sm font-semibold flex gap-1 lg:hidden"
              style={{
                background:
                  "linear-gradient(0deg, rgba(221,221,221,0.4352591036414566) 0%, rgba(245,245,245,0.4100490196078431) 100%)",
              }}
            >
              <span className="self-center">
                <HiStar className="text-[#fffb2a]" />
              </span>
              <span>follow me</span>
            </a>
          </div>
          <div className="pt-3 flex flex-col gap-5 mb-5">
            <div
              ref={section1ref}
              className="flex flex-col lg:flex-row lg:justify-between lg:pt-40 gap-5 lg:gap-0 lg:px-36 lg:min-h-screen overflow-x-hidden"
            >
              {/* tampil saat dekstop */}
              <div className="lg:w-1/2 text-white pt-16 lg:pt-0 px-4 lg:px-0">
                <div className="mb-10">
                  <div className="flex gap-3 mb-4">
                    <h1 className="text-6xl font-extrabold parent-text">
                      <TextAnimation char={"H"} />
                      <TextAnimation char={"i"} />
                    </h1>
                    <img src={Hand} alt="hand" className="w-[60px] h-[60px] " />
                  </div>
                  <div>
                    <p className="font-extrabold text-xl lg:text-5xl mb-3 parent-text">
                      <TextAnimation char={"I"} />
                      <TextAnimation char={"'"} />
                      <TextAnimation char={"m"} />{" "}
                      <span
                        className="text-red-500 text-xl lg:text-5xl"
                        style={{ textShadow: "3px 2px 0px white" }}
                      >
                        R
                      </span>
                      <TextAnimation char={"i"} />
                      <TextAnimation char={"c"} />
                      <TextAnimation char={"k"} />
                      <TextAnimation char={"y"} />
                    </p>
                    <p className="font-extrabold text-xl lg:text-5xl parent-text">
                      {textProfesi.split("").map((item, i) => {
                        if (item === " ") return <> </>;
                        return <TextAnimation key={i} char={item} />;
                      })}
                    </p>
                  </div>
                </div>
                <div
                  className={`${
                    section1IsVisible
                      ? "rotate-0 translate-x-0 -translate-y-0"
                      : "rotate-[360deg] translate-x-[800px] -translate-y-[500px]"
                  } transition duration-1000`}
                >
                  <button
                    type="button"
                    onClick={() => goto(aboutMeRef.current)}
                    className="bg-white group text-red-800 py-2 px-5 rounded font-bold text-sm lg:text-2xl flex gap-2 hover:bg-red-800 hover:text-white transition duration-500 ease-in-out hover:-translate-y-1 hover:scale-110 hover-shadow"
                  >
                    <span>More About What I Do</span>{" "}
                    <span className="self-center bg-red-800 text-white p-1 rounded text-sm group-hover:bg-white group-hover:text-red-800 duration-500">
                      <HiArrowDown />
                    </span>
                  </button>
                </div>
              </div>
              <div className="px-4 flex flex-col gap-5 lg:grid lg:grid-cols-2 lg:w-1/2 lg:h-3/4 pb-3 lg:pb-0">
                <Link
                  to={"/home"}
                  className={`${
                    section1IsVisible
                      ? "translate-x-0 translate-y-0"
                      : "translate-x-[500px] translate-y-[300px]"
                  } bg-[#EAFDFC] relative py-2 rounded-3xl w-full flex justify-center border border-black lg:self-center hover:bg-slate-200 transition duration-1000`}
                  style={{ boxShadow: "7px 11px 2px -2px rgba(0,0,0,0.7)" }}
                >
                  <span className="absolute left-2 top-1">
                    <IoIosHome className="text-red-500 w-[25px] h-[25px]" />
                  </span>
                  <span className="text-slate-800 text-xs font-semibold">
                    Beranda 🏡
                  </span>
                </Link>
                <button
                  onClick={() => goto(projectRef.current)}
                  className={`${
                    section1IsVisible
                      ? "translate-x-0 rotate-0"
                      : "-translate-x-[600px] rotate-[360deg]"
                  } bg-[#EAFDFC] relative py-2 rounded-3xl w-full flex justify-center border border-black lg:self-center hover:bg-slate-200 transition duration-1000`}
                  style={{ boxShadow: "7px 11px 2px -2px rgba(0,0,0,0.7)" }}
                >
                  <span className="absolute left-2 top-1">
                    <HiPencilSquare className="text-sky-900 w-[25px] h-[25px]" />
                  </span>
                  <span className="text-slate-800 text-xs font-semibold">
                    Portfolios ⚔️
                  </span>
                </button>
                <ItemBiodatas
                  text={"Linked In 🔗"}
                  entry="3"
                  href={
                    "https://www.linkedin.com/in/ricky-ramadhani-5231b7242/"
                  }
                  icon={
                    <IoLogoLinkedin className="text-blue-400 w-[25px] h-[25px]" />
                  }
                />
                <ItemBiodatas
                  text={"Gitlab 🦊"}
                  entry="4"
                  href={"https://gitlab.com/rkyrni"}
                  icon={
                    <FaGitlab className="w-[25px] h-[25px] text-orange-500" />
                  }
                />
                <ItemBiodatas
                  text={"Github 🐈"}
                  entry="5"
                  href={"https://github.com/rkyrni"}
                  icon={
                    <IoLogoGithub className="text-black w-[25px] h-[25px]" />
                  }
                />
                <ItemBiodatas
                  text={"Instagram 😎"}
                  entry="6"
                  href="https://www.instagram.com/ricky_rd05/"
                  icon={
                    <IoLogoInstagram
                      className="rounded-full w-[25px] h-[25px]"
                      style={{
                        background:
                          "linear-gradient(186deg, rgba(71,9,186,1) 10%, rgba(157,13,189,1) 48%, rgba(255,217,0,1) 100%)",
                      }}
                    />
                  }
                />
                <ItemBiodatas
                  text={"Whatsapp 🥂"}
                  entry="7"
                  href="https://wa.link/8j3a5b"
                  icon={
                    <IoLogoWhatsapp className="text-green-500 w-[25px] h-[25px]" />
                  }
                />
                <ItemBiodatas
                  text={"Facebook 😅"}
                  entry="1"
                  href="https://web.facebook.com/ricky.ramadhani.7503"
                  icon={
                    <FaFacebook className="text-blue-500 w-[25px] h-[25px]" />
                  }
                />
              </div>
            </div>
          </div>
          {/* About */}
          <div ref={aboutMeRef} className="lg:mb-44 mb-20">
            <AboutMe />
          </div>
          {/* <div ref={projectRef} className="flex justify-center lg:mb-24">
            <button
              type="button"
              onClick={() => goto(projectRef.current)}
              className="mb-2 text-black font-bold animate-bounce flex gap-3 lg:text-4xl"
            >
              <span>Portfolios</span>{" "}
              <span className="self-center">
                <HiArrowDown />
              </span>
            </button>
          </div> */}
          {/* project */}
          {/* new */}
          <Portfolios themeColor={themeColor} />
          {/* end */}
          {/* Skills */}
          <Skills />
          {/* end Skills */}
          {/* <div className="flex justify-center">
            <button
              type="button"
              onClick={() => goto(linkRef.current)}
              className="mb-2 text-black font-bold animate-bounce flex gap-3"
            >
              <span>Tentang Development Web</span>{" "}
              <span className="self-center">
                <HiArrowDown />
              </span>
            </button>
          </div>
          <div className="p-2">
            <div
              ref={linkRef}
              className="bg-white rounded-2xl text-slate-800 py-1 px-2"
            >
              <h2 className="text-center font-bold text-sm">Latar Belakang</h2>
              <br />
              <p className="text-xs mb-4">
                &nbsp;&nbsp;Single Page Application(SPA) ini dibangun dengan
                menggunakan tekhnologi bahasa pemrograman Javascript(EcmaScript
                6) dan sudah Mobile Responsiv.
                <br /> <br />
                &nbsp;&nbsp;Dengan menggunakan <strong>ReactJs</strong> sebagai
                pembentuk Frontend(User Interface), Dipadukan dengan beberapa
                framework sebagai support untuk mendukung{" "}
                <strong>ReactJs</strong> menjadi lebih reactive.
                <br />
                Adapun beberapa framework tersebut adalah :
                <br />
                &nbsp;- Tailwind (Styling),
                <br />
                &nbsp;- Redux Toolkit (Global State Management),
                <br />
                &nbsp;- js-cookie (Browser Storage)
                <br />
                &nbsp;- React-router-dom (Router)
                <br />
                &nbsp;- Socket.io-client (Realtime Database).
                <br /> <br />
                &nbsp;&nbsp;Dan untuk Backend nya sendiri dibangun dengan
                tekhnologi <strong>NodeJs</strong> dengan beberapa framework
                sebagai support/middleware.
                <br />
                Adapun beberapa framework tersebut adalah :
                <br />
                &nbsp;- Express Js
                <br />
                &nbsp;- Passport (Authentication)
                <br />
                &nbsp;- PostgreSQL (DBMS)
                <br />
                &nbsp;- Cloudinary (Cloud Database)
                <br />
                &nbsp;- Socket.io (Realtime Database)
                <br />
                <br />
                &nbsp;&nbsp;Pada Web App ini terdapat fitur beberapa{" "}
                <strong>permainan Game sederhana</strong> yang dapat dimainkan
                dengan torehan skor yang akan tercatat dan memiliki papan
                peringkatnya masing-masing di setiap game nya.
                <br />
                &nbsp;&nbsp;Fitur lainnya dari Web App ini adalah{" "}
                <strong>pembuatan Resume(CV)</strong> yang sangat mudah dan
                dengan pilihan template yang sudah tersedia, nantinya User hanya
                perlu mengisi form yang tersedia dan langsung dapat men-download
                Resume(CV) tersebut dalam format pdf.
                <br />
                &nbsp;&nbsp;Selain itu User juga dapat berkomunikasi dengan
                Admin melalui fitur <strong>Realtime Chat</strong> yang telah
                tersedia.
              </p>
              <div
                className="text-end text-blue-500 italic text-xs cursor-pointer"
                onClick={() => goto(headRef.current)}
              >
                kembali ke atas
              </div>
            </div>
          </div> */}
          <Certificate />
          <FooterAbout />
        </div>
      </div>
    </div>
  );
};

export default AboutDeveloper;
