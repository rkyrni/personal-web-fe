import React from "react";
import { HiBookOpen, HiBriefcase } from "react-icons/hi2";

const Steps = () => {
  const ItemSteps = ({
    status,
    color,
    profesi,
    date,
    company,
    icon,
    iconColor,
  }) => {
    return (
      <div className="flex mb-10">
        <div className="flex flex-col items-center mr-4">
          <div>
            <div className="flex items-center justify-center w-10 h-10 border border-slate-800 rounded-full">
              <svg
                className="w-4 text-gray-600"
                stroke="currentColor"
                strokeWidth="3"
                strokeLinecap="round"
                strokeLinejoin="round"
                viewBox="0 0 24 24"
              >
                <line
                  fill="none"
                  strokeMiterlimit="10"
                  x1="12"
                  y1="2"
                  x2="12"
                  y2="22"
                />
                <polyline
                  fill="none"
                  strokeMiterlimit="10"
                  points="19,15 12,22 5,15"
                />
              </svg>
            </div>
          </div>
          <div className="w-px h-full bg-gray-300" />
        </div>
        <div className="pb-8">
          <div className="flex justify-center m-auto lg:w-1/4 lg:mx-6 -mt-12">
            <div className="relative w-64 h-48">
              <div className="absolute top-0 left-0 flex items-center w-52 h-44 lg:w-96 lg:h-40 mt-6 ml-6  bg-white border-8 border-gray-700 border-solid rounded-lg">
                <div className="w-1/3 h-40 hidden lg:block"></div>
                <div className="w-full lg:w-2/3 h-32 pr-0 lg:pr-4 pl-6 lg:pl-0">
                  <h3 className="pt-1 text-xl font-semibold text-gray-700">
                    {status}
                  </h3>
                  <p className="pt-1 text-sm text-gray-600 font-bold">
                    {profesi}
                  </p>
                  <p className="pt-1 text-sm text-gray-600">{date}</p>
                  <p className="pt-1 text-sm text-gray-600">{company}</p>
                </div>
              </div>

              <div
                className={`absolute top-0 left-0 z-10 w-12 h-20 lg:w-24 lg:h-40 py-4 lg:py-20 text-5xl font-bold text-center text-white ${color} rounded-lg flex justify-center`}
              >
                <div
                  className={`bg-white rounded-full flex justify-center p-2 self-center`}
                >
                  <span className={`${iconColor} text-xs lg:text-4xl`}>
                    {icon}
                  </span>
                </div>
              </div>
              <div
                className={`absolute top-0 left-0 z-30 w-44 lg:w-60 h-2 mt-44 lg:mt-40 ml-20 lg:ml-48 ${color}`}
              ></div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
      <div className="flex justify-center">
        <div className="lg:py-6 lg:pr-16">
          <ItemSteps
            status={"Course"}
            profesi={"React Js Web Frontend"}
            date={"Mar 2022 - Apr 2022"}
            color={"bg-[#61e2ff]"}
            iconColor={"text-[#61e2ff]"}
            company={"Sanbercode"}
            icon={<HiBookOpen />}
          />
          <ItemSteps
            status={"Work"}
            profesi={"React Js Developer (Freelancer)"}
            date={"Oct 2022 - now"}
            color={"bg-[#abcc72]"}
            iconColor={"text-[#abcc72]"}
            company={"Pt.Insyst Media Solutions"}
            icon={<HiBriefcase />}
          />
          <ItemSteps
            status={"Course"}
            profesi={"Full-Stack Developer"}
            date={"Jun 2022 - Dec 2022"}
            color={"bg-[#856efa]"}
            iconColor={"text-[#856efa]"}
            company={"Binar Academy"}
            icon={<HiBookOpen />}
          />
        </div>
      </div>
    </div>
  );
};

export default Steps;
