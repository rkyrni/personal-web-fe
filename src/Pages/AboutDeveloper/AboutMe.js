import React from "react";
import { useInView } from "react-intersection-observer";
import Picture from "../../assets/img/pic-1.jpg";

const AboutMe = () => {
  const { ref: aboutMeRef, inView: aboutMeIsVisible } = useInView();
  return (
    <div
      ref={aboutMeRef}
      className="flex flex-col lg:flex-row justify-evenly gap-3 bg-[#00ADB5] text-white py-16 overflow-hidden"
    >
      <div className="self-center lg:self-start">
        <figure className={`self-center relative`}>
          <div
            className={`bg-white p-1 absolute -ml-8 mt-2 rounded-lg -rotate-12`}
          >
            <div className="w-[100px] h-[100px] lg:w-[200px] rounded-lg lg:h-[200px] bg-slate-800"></div>
          </div>
          <div className="bg-white p-1 z-40 rounded-lg relative">
            <img
              className={`${
                aboutMeIsVisible
                  ? "translate-x-0 opacity-100"
                  : "-translate-x-[200px] opacity-0"
              } w-[100px] h-[100px] lg:w-[200px] lg:h-[200px] rounded-lg transition duration-1000`}
              src={Picture}
              alt="pic"
            />
          </div>
        </figure>
      </div>
      <div className="w-full lg:w-1/2 self-center lg:self-start">
        <h3
          className={`${
            aboutMeIsVisible
              ? "translate-x-0 opacity-100"
              : "-translate-x-[200px] opacity-0"
          } font-bold text-4xl mb-4 text-center lg:text-start transition duration-1000`}
        >
          About Me
        </h3>
        <p
          className={`${
            aboutMeIsVisible
              ? "translate-x-0 opacity-100"
              : "translate-x-[300px] opacity-0"
          } lg:w-3/4 w-full px-2 text-center lg:text-start transition duration-1000`}
        >
          My name is Ricky Ramadani, I am a Software Developer with skills in
          Javascript, React Js, .NET, Node Js, and other web technologies.
          <br />
          Currently I work in{" "}
          <a
            className="text-blue-700 italic underline"
            href="https://genesisdigital.asia/"
            target="_blank"
            rel="noreferrer"
          >
            Pt.Genesis Digital Asia
          </a>{" "}
          Company as a Software Developer. I also work as a freelancer on
          several websites and work remotely. I am passionate about developing
          websites that are user-friendly, visually appealing, and optimized for
          performance. I am very motivated to keep learning every day and keep
          abreast of the latest trends in the industry so that the skills I have
          can be better than yesterday.
        </p>
      </div>
    </div>
  );
};

export default AboutMe;
