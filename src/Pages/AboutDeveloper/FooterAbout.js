import React from "react";

const FooterAbout = () => {
  return (
    <div className="py-1 lg:py-4 bg-[#EAFDFC] text-black font-semibold">
      <div className="text-xs lg:text-sm text-center">
        <p className="ext-center hidden lg:block">
          © 2023 - Ricky Ramadani | rickyramadhani737@gmail.com | Labuhanbatu
          Selatan, Sumatera Utara
        </p>
        <p className="block lg:hidden">© 2023 - Ricky Ramadani</p>
        <p className="block lg:hidden">rickyramadhani737@gmail.com</p>
        <p className="block lg:hidden">Labuhanbatu Selatan, Sumatera Utara</p>
      </div>
    </div>
  );
};

export default FooterAbout;
