import React from "react";

import { dataSkill, dataSkill2, dataSkill3 } from "./dataskills";

const Skills = () => {
  const AnimationSkills = ({ className, data }) => {
    return (
      <div
        className="flex flex-nowrap gap-3 lg:gap-10 overflow-x-hidden justify-evenly"
        style={{ transitionDuration: "10s" }}
      >
        {data.map((item, i) => {
          return (
            <div
              key={i}
              className={`bg-slate-800 text-slate-200 px-5 py-1 rounded-lg shadow-lg text-xs lg:text-xl pb-2 min-w-max ${className}`}
            >
              <span className="self-center">{item}</span>
            </div>
          );
        })}
      </div>
    );
  };

  return (
    <div className="mb-20 lg:px-56">
      <div>
        <div className="px-2 lg:px-0">
          <div className="mb-8 text-center lg:text-start">
            <h3 className="lg:text-4xl text-2xl font-bold">Skills </h3>
          </div>
          <div className="flex flex-col gap-8">
            <AnimationSkills data={dataSkill2} className={"skills"} />
            <AnimationSkills data={dataSkill} className={"skills-reverse"} />
            <AnimationSkills data={dataSkill3} className={"skills"} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Skills;
