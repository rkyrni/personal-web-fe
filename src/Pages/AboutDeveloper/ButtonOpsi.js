import React, { useState } from "react";
import { HiPlus } from "react-icons/hi2";
import { FaLaptopCode } from "react-icons/fa";
import { HiMinusCircle, HiChat } from "react-icons/hi";

const ButtonOpsi = ({ setButton_1_show, setButton_2_show }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [isRocket, setIsRocket] = useState(false);

  return (
    <div
      className={`${
        isOpen ? "" : ""
      } fixed right-10 bottom-4 lg:right-16 lg:bottom-10 rounded-full`}
      style={{ boxShadow: "0 10px 20px rgba(0, 0, 0, 0.1)", zIndex: 700 }}
    >
      <button
        className={`${
          isOpen ? "rotate-45" : " rotate-0"
        } cursor-pointer relative z-10 grid place-items-center lg:w-[50px] lg:h-[50px] w-[50px] h-[50px] rounded-full border-0 bg-[#1D91EA] text-[#F9F9F9] transition duration-200`}
        onClick={() => setIsOpen(!isOpen)}
      >
        <HiPlus />
      </button>
      <div
        className={`${
          isOpen
            ? "rotate-0 scale-[1] opacity-100"
            : "-rotate-[220deg] scale-[0.7] opacity-0"
        } absolute z-0 top-1/2 left-1/2 w-[200px] h-[200px] -translate-x-1/2 -translate-y-1/2 border-[80px] rounded-full transition ease-out duration-500`}
        style={{ borderColor: "rgba(255, 255, 255, 0.5)" }}
      >
        <button
          onClick={() => setIsRocket(true)}
          className={`${isOpen ? "-rotate-[100deg]" : "rotate-45"} ${
            isRocket ? "animate-rocket" : ""
          } bg-transparent border-0 cursor-pointer absolute text-slate-800 transition duration-300 hover:scale-[1.2] left-[30px] -top-[64px]`}
        >
          <span className="text-[32px] text-red-500">🚀</span>
        </button>
        <button
          onClick={() => {
            setButton_1_show(true);
            setIsOpen(false);
          }}
          className={`${
            isOpen ? "rotate-0" : "rotate-45"
          } bg-transparent border-0 cursor-pointer absolute text-slate-800 transition duration-300 -left-[130px] -top-[48px] group flex`}
        >
          <span className="transition self-center text-sm group-hover:opacity-100 group-hover:translate-x-0 duration-200 translate-x-8 bg-slate-800 py-2 text-white opacity-0 pl-2 rounded-l-lg">
            professional&nbsp;&nbsp;
          </span>
          <span className="text-[32px] bg-slate-800 text-white rounded-full group-hover:rounded-l-none p-2 self-center text-xs">
            <FaLaptopCode className="text-xl" />
          </span>
        </button>
        <button
          onClick={() => {
            setButton_2_show(true);
            setIsOpen(false);
          }}
          className={`${
            isOpen ? "rotate-0" : "rotate-45"
          }  bg-transparent border-0 cursor-pointer absolute text-slate-800 transition duration-300 -left-[195px] top-[20px] group flex`}
        >
          <span className="transition self-center text-sm group-hover:opacity-100 group-hover:translate-x-0 duration-200 translate-x-8 bg-slate-800 py-2 text-white opacity-0 pl-2 rounded-l-lg min-w-max">
            anonym messages&nbsp;&nbsp;
          </span>
          <span className="text-[32px] bg-slate-800 text-white rounded-full group-hover:rounded-l-none p-2 self-center text-xs">
            <HiChat className="text-xl" />
          </span>
        </button>
      </div>
    </div>
  );
};

export default ButtonOpsi;
