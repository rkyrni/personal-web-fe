import React, { useState } from "react";
import { useEffect } from "react";

const ModalAbout = ({ tittle, body, footer, setIsModalView }) => {
  const [animationModal, setAnimationModal] = useState(false);

  useEffect(() => {
    setAnimationModal(true);
  }, []);

  return (
    <div
      style={{ backgroundColor: "rgba(0,0,0,0.5)", zIndex: 800 }}
      className="fixed top-0 left-0 right-0 bottom-0 self-center min-h-screen flex justify-center"
      onClick={() => {
        setAnimationModal(false);
        setTimeout(() => {
          setIsModalView(false);
        }, 200);
      }}
    >
      <div
        className={`${
          animationModal ? "translate-y-0" : "-translate-y-[900px]"
        } modal-box max-w-none w-full lg:w-4/5 relative self-center bg-[#EAFDFC] z-[850] text-slate-800 transition duration-700`}
      >
        <div
          onClick={() => {
            setAnimationModal(false);
            setTimeout(() => {
              setIsModalView(false);
            }, 200);
          }}
          className="btn btn-sm btn-circle absolute right-2 top-2 text-red-500 bg-transparent border-none font-bold"
        >
          ✕
        </div>
        <h3 className="text-lg font-bold text-center lg:text-3xl">{tittle}</h3>
        <div className="">{body}</div>
        <div className="">{footer}</div>
      </div>
    </div>
  );
};

export default ModalAbout;
