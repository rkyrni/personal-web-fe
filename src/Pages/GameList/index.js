import axios from "axios";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Footer from "../../Components/Footer";
import Loading from "../../Components/Loading";
import Navbar from "../../Components/Navbar";
import ItemGameList from "./ItemGameList";

const GameList = () => {
  const [gameList, setGameList] = useState(null);
  const [fecth, setFecth] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const themeMode = useSelector((state) => state.themeMode.value);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_BASE_URL}/game-list`
        );
        setGameList(response.data.data);
        setIsLoading(false);
      } catch (error) {
        alert(error);
        setIsLoading(false);
      }
    };

    if (fecth) {
      fetchData();
      setFecth(false);
    }
  }, [fecth]);

  return (
    <div className="">
      {isLoading ? <Loading /> : null}
      <Navbar />
      <div
        className={`${
          themeMode ? "bg-black text-slate-200" : "bg-slate-200 text-slate-800"
        } min-h-screen`}
      >
        <div className="pt-16 lg:pt-24 mb-4">
          <div className="flex flex-col gap-5 lg:grid lg:grid-cols-3 lg:justify-items-center">
            {gameList ? (
              <>
                {gameList.map((item, i) => {
                  return <ItemGameList key={i} item={item} />;
                })}
              </>
            ) : null}
          </div>
        </div>
        <Footer />
      </div>
    </div>
  );
};

export default GameList;
