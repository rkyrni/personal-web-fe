import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const ItemGameList = ({ item }) => {
  const themeMode = useSelector((state) => state.themeMode.value);
  return (
    <div
      className={`${
        themeMode
          ? "bg-slate-800 text-slate-200 shadow-md shadow-white"
          : "bg-white shadow-lg text-slate-800"
      } self-center w-4/5 lg:w-2/3 rounded-lg p-2 flex flex-col gap-3`}
    >
      <div className="relative">
        {item?.isAlready ? null : (
          <div className="bg-red-700 text-white absolute font-semibold px-2 py-1 bottom-6 left-0 rounded-r-lg text-sm shadow-lg">
            Coming Soon
          </div>
        )}
        <img className="w-full h-[200px]" src={item.image_url} alt="game" />
      </div>
      <div className="flex flex-col gap-4">
        <div>
          <h2 className="text-center font-semibold mb-3">{item.name}</h2>
          <p className="text-xs text-center">{item.description}</p>
        </div>
        <div className="flex justify-end">
          {item?.isAlready ? (
            <Link
              className="text-xs bg-blue-500 text-white px-2 py-1 rounded-lg border border-white"
              to={`/game-detail/${item.label}`}
            >
              buka game
            </Link>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default ItemGameList;
