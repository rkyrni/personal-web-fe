import React, { useEffect, useState } from "react";
import axios from "axios";
import { passwordStrength } from "check-password-strength";
import {
  HiOutlineEnvelopeOpen,
  HiOutlineLockClosed,
  HiUser,
  HiOutlineFaceSmile,
} from "react-icons/hi2";
import { Link, useNavigate } from "react-router-dom";
import Background from "../../assets/img/bg-3.jpg";
import Loading from "../../Components/Loading";

const Register = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [textHeader, setTextHeader] = useState("Register Form");
  const [progressBar, setProgressBar] = useState(25);
  const [triggerReactive, setTriggerRactive] = useState(false);
  const [passwordValidation, setPasswordValidation] = useState(null);
  const [input, setInput] = useState({
    name: "",
    email: "",
    password: "",
  });

  const navigate = useNavigate();

  useEffect(() => {
    setPasswordValidation(passwordStrength(input.password));
  }, [progressBar, input]);

  useEffect(() => {
    if (passwordValidation) {
      indicatorProgress();
    }
  }, [triggerReactive, passwordValidation]);

  const textManipulation = () => {
    setTimeout(() => {
      setTextHeader("Register Form");
    }, 5000);
  };

  const handlerSubmit = async (e) => {
    e.preventDefault();

    if (progressBar < 50) {
      setTextHeader("Password lemah");
      textManipulation();
      return;
    }

    setIsLoading(true);
    try {
      input.email = input.email.toLowerCase();
      await axios.post(`${process.env.REACT_APP_BASE_URL}/register`, input);
      setIsLoading(false);
      navigate("/login");
    } catch (error) {
      if (error.response.status === 402) {
        setTextHeader("Email sudah dipakai!");
        textManipulation();
      } else {
        alert(error);
      }

      setIsLoading(false);
    }
  };

  const handlerChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
    setPasswordValidation(passwordStrength(input.password));
    setTriggerRactive(!triggerReactive);
  };

  const indicatorProgress = () => {
    if (passwordValidation.length < 6) {
      setProgressBar(25);
    } else if (
      passwordValidation.contains.includes("lowercase") &&
      passwordValidation.contains.includes("uppercase") &&
      passwordValidation.contains.includes("number") &&
      passwordValidation.length >= 6
    ) {
      setProgressBar(100);
    } else if (
      passwordValidation.contains.includes("lowercase") &&
      passwordValidation.contains.includes("uppercase") &&
      passwordValidation.length >= 6
    ) {
      setProgressBar(75);
    } else if (
      passwordValidation.contains.includes("lowercase") &&
      passwordValidation.contains.includes("number") &&
      passwordValidation.length >= 6
    ) {
      setProgressBar(75);
    } else {
      setProgressBar(50);
    }
  };

  const ProgressBar = () => {
    let typeBar = "progress-error";

    if (progressBar >= 25) typeBar = "progress-error";
    if (progressBar >= 50) typeBar = "progress-warning";
    if (progressBar >= 75) typeBar = "progress-success";

    return (
      <progress
        className={`progress ${typeBar} w-56 bg-slate-800 lg:w-full`}
        value={`${progressBar}`}
        max="100"
      ></progress>
    );
  };

  return (
    <div
      style={{ backgroundImage: `url(${Background})` }}
      className="flex justify-center min-h-screen bg-cover"
    >
      {isLoading ? <Loading /> : null}
      <div
        style={{
          boxShadow: "0px 0px 25px 10px black",
          background: "rgba(4, 29, 23, 0.5)",
        }}
        className="self-center py-3 px-8 rounded-lg flex flex-col gap-5 lg:w-1/4 w-3/4"
      >
        <div className="flex justify-center -mt-8">
          <div
            style={{ boxShadow: "0px 0px 6px 5px black" }}
            className="w-max p-3 rounded-full bg-black text-white"
          >
            <HiUser />
          </div>
        </div>
        <h2
          className={`${
            textHeader !== "Register Form" ? "text-red-500" : "text-slate-300"
          } text-2xl text-center font-bold mb-6 animate-bounce`}
        >
          {textHeader}
        </h2>
        <form
          className="flex flex-col gap-6"
          action=""
          method="post"
          onSubmit={handlerSubmit}
        >
          <div className="flex gap-3 border-b-2 border-slate-700 pb-1">
            <span className="self-center">
              <HiOutlineFaceSmile />
            </span>
            <input
              className="bg-transparent text-white font-semibold px-2"
              name="name"
              onChange={handlerChange}
              type="text"
              placeholder="nama"
              required
              value={input.name}
            />
          </div>
          <div className="flex gap-3 border-b-2 border-slate-700 pb-1">
            <span className="self-center">
              <HiOutlineEnvelopeOpen />
            </span>
            <input
              className="bg-transparent text-white font-semibold px-2"
              required
              name="email"
              onChange={handlerChange}
              type="email"
              placeholder="email"
              value={input.email}
            />
          </div>
          <div className="flex gap-3 border-b-2 border-slate-700 pb-1">
            <span className="self-center">
              <HiOutlineLockClosed />
            </span>
            <input
              className="bg-transparent text-white font-semibold px-2"
              name="password"
              onChange={handlerChange}
              type="password"
              required
              placeholder="password (Contoh123)"
              value={input.password}
            />
          </div>
          {input.password.length !== 0 ? <ProgressBar /> : null}
          <button
            className="border border-slate-700 py-2 font-semibold hover:shadow-2xl hover:shadow-black text-white"
            type="submit"
          >
            Register
          </button>
        </form>
        <div className="mt-5 flex justify-end">
          <Link className="italic text-blue-500" to="/login">
            Sudah punya akun!
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Register;
