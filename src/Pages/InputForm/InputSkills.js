import React, { useEffect, useState } from "react";
import { HiOutlineTrash, HiOutlinePlus } from "react-icons/hi2";
import { useDispatch, useSelector } from "react-redux";
import { onChangeInput } from "../../redux/reducer/inputFormCv";

const InputSkills = () => {
  const [inputSkillsLength, setInputSkillsLength] = useState(1);
  const [alertAddInput, setAlertAddInput] = useState(false);

  const inputFormCv = useSelector((state) => state.inputFormCv.value);
  const themeMode = useSelector((state) => state.themeMode.value);
  const dispatch = useDispatch();

  useEffect(() => {
    if (inputFormCv.skills6) setInputSkillsLength(6);
    else if (inputFormCv.skills5) setInputSkillsLength(5);
    else if (inputFormCv.skills4) setInputSkillsLength(4);
    else if (inputFormCv.skills3) setInputSkillsLength(3);
    else if (inputFormCv.skills2) setInputSkillsLength(2);
  }, []);

  const result = [];
  for (let i = 1; i <= inputSkillsLength; i++) {
    let temp = (
      <div
        key={i}
        className={`${
          themeMode ? "text-white" : "text-slate-800"
        } flex flex-col gap-2 mb-5`}
      >
        <div className="flex gap-1">
          <p className="text-xs self-center">Kemampuan {i} :</p>
          <input
            type="text"
            required={i === 1}
            name={`skills${i}`}
            maxLength={20}
            value={inputFormCv[`skills${i}`]}
            onChange={(e) => dispatch(onChangeInput(e.target))}
            placeholder="Max 20 character"
            className={`${
              alertAddInput && inputSkillsLength === i
                ? "border-red-700"
                : "border-yellow-500"
            } bg-slate-200 rounded border px-2`}
          />
        </div>
        <select
          name={`skillsPercentage${i}`}
          required={i === 1}
          defaultValue={inputFormCv[`skillsPercentage${i}`]}
          onChange={(e) => dispatch(onChangeInput(e.target))}
          className={`${
            alertAddInput && inputSkillsLength === i
              ? "bg-red-500"
              : "bg-yellow-500"
          } select text-xs w-1/2 self-end text-white`}
        >
          <option value={""} disabled>
            Pilih Persentase
          </option>
          <option value={"10"}>10%</option>
          <option value={"30"}>30%</option>
          <option value={"60"}>60%</option>
          <option value={"70"}>70%</option>
          <option value={"80"}>80%</option>
          <option value={"90"}>90%</option>
          <option value={"95"}>95%</option>
          <option value={"100"}>100%</option>
        </select>
        <div className="flex pl-8 gap-5">
          {inputSkillsLength === i && inputSkillsLength !== 1 ? (
            <button
              type="button"
              onClick={() => {
                setInputSkillsLength(inputSkillsLength - 1);
                dispatch(onChangeInput({ name: `skills${i}`, value: "" }));
                dispatch(
                  onChangeInput({ name: `skillsPercentage${i}`, value: "" })
                );
              }}
              className="bg-slate-700 p-2 rounded-full text-white"
            >
              <HiOutlineTrash />
            </button>
          ) : null}
          {inputSkillsLength < 6 && inputSkillsLength === i ? (
            <button
              type="button"
              onClick={() => {
                if (
                  !inputFormCv[`skills${i}`] ||
                  !inputFormCv[`skillsPercentage${i}`]
                ) {
                  setAlertAddInput(true);
                } else {
                  setInputSkillsLength(inputSkillsLength + 1);
                  setAlertAddInput(false);
                }
              }}
              className={`${
                themeMode
                  ? "border-white text-white"
                  : "border-slate-700 text-slate-700"
              } border  p-2 rounded-full `}
            >
              <HiOutlinePlus />
            </button>
          ) : null}
        </div>
      </div>
    );

    result.push(temp);
  }

  return result;
};

export default InputSkills;
