import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { HiOutlineTrash } from "react-icons/hi2";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Milea from "../../assets/img/milea.jpg";
import Modal from "../../Components/Modal";
import Navbar from "../../Components/Navbar";
import {
  deleteValueInput,
  onChangeInput,
} from "../../redux/reducer/inputFormCv";
import InputBiodata from "./InputBiodata";
import InputEducation from "./InputEducation";
import InputSkills from "./InputSkills";

const InputForm = () => {
  const [imageFile, setImageFile] = useState(null);
  const [isModalView, setIsModalView] = useState(false);
  const inputFormCv = useSelector((state) => state.inputFormCv.value);

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const themeMode = useSelector((state) => state.themeMode.value);

  useEffect(() => {
    if (Cookies.get("profileResume") && !imageFile)
      setImageFile(Cookies.get("profileResume"));
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!imageFile) {
      alert("Foto nya belum di isi!");
      return;
    }
    Cookies.set("profileResume", imageFile);
    Cookies.set("inputForm", inputFormCv.name);
    Cookies.set("inputFormCv", JSON.stringify(inputFormCv));
    // console.log(inputFormCv);
    dispatch(onChangeInput({ name: "image", value: imageFile }));
    navigate("/pengalaman-kerja");
  };

  const FooterModal = () => {
    return (
      <div className="flex justify-around">
        <button
          onClick={() => setIsModalView(false)}
          className="bg-red-400 py-2 px-3 rounded-lg text-black"
          type="button"
        >
          Batal
        </button>
        <button
          onClick={() => {
            dispatch(deleteValueInput());
            setImageFile(null);
            setIsModalView(false);
            Cookies.remove("inputFormCv");
            Cookies.remove("profileResume");
          }}
          className="bg-green-400 py-2 px-3 rounded-lg text-white"
          type="button"
        >
          Oke
        </button>
      </div>
    );
  };

  return (
    <div
      className={`${
        themeMode ? "bg-black text-slate-100" : "bg-slate-200 text-slate-800"
      } min-h-screen pt-16`}
    >
      <Navbar />
      {isModalView ? (
        <Modal
          tittle={"Menghapus Seluruh isi Form ?"}
          footer={<FooterModal />}
          setIsModalView={setIsModalView}
        />
      ) : null}
      <form
        action="POST"
        method="POST"
        onSubmit={handleSubmit}
        className="flex flex-col gap-5"
      >
        <div className="bg-yellow-600 flex justify-center py-2 rounded-lg">
          <label
            htmlFor="image"
            className="bg-slate-200 relative w-[150px] h-[225px] rounded flex flex-col text-center justify-center text-2xl border border-dashed border-slate-600 text-white"
          >
            <img
              className="absolute h-full"
              src={!imageFile ? Milea : imageFile}
              alt="milea"
            />
            <span hidden={imageFile} className="text-xs z-30">
              Your photo here
            </span>
            <span hidden={imageFile} className="z-30">
              6 x 4
            </span>
          </label>
          <input
            id="image"
            type="file"
            name="image"
            onChange={(e) => {
              const [file] = e.target.files;
              if (file) {
                setImageFile(URL.createObjectURL(file));
              }
            }}
            className="hidden"
          />
        </div>
        <InputBiodata />
        <div className="px-4">
          <div
            className={`${
              themeMode
                ? "bg-slate-800 text text-slate-200 shadow-sm shadow-white"
                : "bg-white text-slate-800"
            } rounded-lg shadow-lg p-2`}
          >
            <p className="font-semibold">Deskripsi Diri :</p>
            <textarea
              className="w-full bg-slate-200 rounded border border-yellow-500 text-slate-800 px-2"
              name="about"
              required
              cols="30"
              rows="5"
              value={inputFormCv.about}
              placeholder="Deskripsi diri..."
              onChange={(e) => dispatch(onChangeInput(e.target))}
            ></textarea>
          </div>
        </div>
        <div className="px-4">
          <div
            className={`${
              themeMode
                ? "bg-slate-800 text text-slate-200 shadow-sm shadow-white"
                : "bg-white text-slate-800"
            } rounded-lg shadow-lg p-2`}
          >
            <p className="font-semibold">Kemampuan :</p>
            <InputSkills />
          </div>
        </div>
        <div className="px-1">
          <div
            className={`${
              themeMode
                ? "bg-slate-800 text text-slate-200 shadow-sm shadow-white"
                : "bg-white text-slate-800"
            } rounded-lg shadow-lg p-2`}
          >
            <p className="font-semibold">Pendidikan :</p>
            <InputEducation />
          </div>
        </div>
        <div className="p-3">
          <button
            className="btn btn-active btn-accent text-xs w-full"
            type="submit"
          >
            submit
          </button>
        </div>
        <div className="p-3">
          <button
            onClick={() => setIsModalView(true)}
            className="btn btn-error text-xs rounded-full mb-20"
            type="button"
          >
            <HiOutlineTrash />
          </button>
        </div>
      </form>
    </div>
  );
};

export default InputForm;
