import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { onChangeInput } from "../../redux/reducer/inputFormCv";

const InputBiodata = () => {
  const inputFormCv = useSelector((state) => state.inputFormCv.value);
  const themeMode = useSelector((state) => state.themeMode.value);
  const dispatch = useDispatch();

  return (
    <div className="px-4">
      <div
        className={`${
          themeMode
            ? "bg-slate-800 text text-slate-200 shadow-sm shadow-white"
            : "bg-white text-slate-800"
        } rounded-lg shadow-lg p-3 flex flex-col gap-2`}
      >
        <div className="flex gap-2 justify-between">
          <p className="font-semibold">Nama :</p>
          <input
            type="text"
            name="name"
            value={inputFormCv.name}
            onChange={(e) => dispatch(onChangeInput(e.target))}
            required
            placeholder="nama..."
            className="w-3/4 bg-slate-200 rounded border border-yellow-500 text-slate-800 px-2"
          />
        </div>
        <div className="flex gap-2 justify-between">
          <p className="font-semibold">Email :</p>
          <input
            type="text"
            name="email"
            value={inputFormCv.email}
            onChange={(e) => dispatch(onChangeInput(e.target))}
            required
            placeholder="example@gmail.com"
            className="w-3/4 bg-slate-200 rounded border border-yellow-500 text-slate-800 px-2"
          />
        </div>
        <div className="flex gap-2 justify-between">
          <p className="font-semibold">No.telp :</p>
          <input
            type="text"
            name="phone"
            value={inputFormCv.phone}
            onChange={(e) => dispatch(onChangeInput(e.target))}
            required
            placeholder="08..."
            className="w-3/4 bg-slate-200 rounded border border-yellow-500 text-slate-800 px-2"
          />
        </div>
        <div className="flex gap-2 justify-between">
          <p className="font-semibold">Alamat :</p>
          <input
            type="text"
            name="address"
            value={inputFormCv.address}
            onChange={(e) => dispatch(onChangeInput(e.target))}
            required
            placeholder="alamat..."
            className="w-3/4 bg-slate-200 rounded border border-yellow-500 text-slate-800 px-2"
          />
        </div>
      </div>
    </div>
  );
};

export default InputBiodata;
