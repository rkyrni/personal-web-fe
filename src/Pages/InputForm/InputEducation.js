import React, { useEffect, useState } from "react";
import { HiOutlineTrash, HiOutlinePlus } from "react-icons/hi2";
import { useDispatch, useSelector } from "react-redux";
import { onChangeInput } from "../../redux/reducer/inputFormCv";

const InputEducation = () => {
  const [inputEducationLength, setInputEducationLength] = useState(1);
  const [alertAddInput, setAlertAddInput] = useState(false);
  const result = [];

  const inputFormCv = useSelector((state) => state.inputFormCv.value);
  const themeMode = useSelector((state) => state.themeMode.value);
  const dispatch = useDispatch();

  useEffect(() => {
    if (inputFormCv.educationName6) setInputEducationLength(6);
    else if (inputFormCv.educationName5) setInputEducationLength(5);
    else if (inputFormCv.educationName4) setInputEducationLength(4);
    else if (inputFormCv.educationName3) setInputEducationLength(3);
    else if (inputFormCv.educationName2) setInputEducationLength(2);
  }, []);

  const handleAddInput = (i) => {
    if (
      !inputFormCv[`educationName${i}`] ||
      !inputFormCv[`jurusan${i}`] ||
      !inputFormCv[`fromEducation${i}`] ||
      !inputFormCv[`toEducation${i}`]
    ) {
      setAlertAddInput(true);
    } else {
      setInputEducationLength(inputEducationLength + 1);
      setAlertAddInput(false);
    }
  };

  const handleDeleteInput = (i) => {
    setInputEducationLength(inputEducationLength - 1);
    dispatch(onChangeInput({ name: `educationName${i}`, value: "" }));
    dispatch(onChangeInput({ name: `jurusan${i}`, value: "" }));
    dispatch(onChangeInput({ name: `fromEducation${i}`, value: "" }));
    dispatch(onChangeInput({ name: `toEducation${i}`, value: "" }));
  };

  for (let i = 1; i <= inputEducationLength; i++) {
    let temp = (
      <div
        key={i}
        className={`${
          themeMode ? "text-white" : "text-slate-800"
        } flex flex-col gap-1 mb-5`}
      >
        <p className="text-xs">pendidikan {i} :</p>
        <div className="flex justify-between">
          <p className="text-xs">Nama :</p>
          <input
            type="text"
            required={i === 1}
            name={`educationName${i}`}
            value={inputFormCv[`educationName${i}`]}
            onChange={(e) => dispatch(onChangeInput(e.target))}
            placeholder="Universitas ..."
            className={`${
              alertAddInput && inputEducationLength === i
                ? "border-red-500"
                : "border-yellow-500"
            } bg-slate-200 rounded border text-slate-800 px-2 w-4/5`}
          />
        </div>
        <div className="flex justify-between">
          <p className="text-xs">Jurusan :</p>
          <input
            type="text"
            name={`jurusan${i}`}
            required={i === 1}
            value={inputFormCv[`jurusan${i}`]}
            onChange={(e) => dispatch(onChangeInput(e.target))}
            placeholder="Kimia Industri ..."
            className={`${
              alertAddInput && inputEducationLength === i
                ? "border-red-500"
                : "border-yellow-500"
            } bg-slate-200 rounded border text-slate-800 px-2 w-4/5`}
          />
        </div>
        <div className="flex justify-between">
          <p className="text-xs">Dari :</p>
          <input
            type="date"
            required={i === 1}
            name={`fromEducation${i}`}
            value={inputFormCv[`fromEducation${i}`]}
            onChange={(e) => dispatch(onChangeInput(e.target))}
            className={`${
              alertAddInput && inputEducationLength === i
                ? "border-red-500"
                : "border-yellow-500"
            } bg-slate-200 rounded border text-slate-800 px-2 w-4/5`}
          />
        </div>
        <div className="flex justify-between">
          <p className="text-xs">Sampai :</p>
          <input
            type="date"
            required={i === 1}
            name={`toEducation${i}`}
            value={inputFormCv[`toEducation${i}`]}
            onChange={(e) => dispatch(onChangeInput(e.target))}
            className={`${
              alertAddInput && inputEducationLength === i
                ? "border-red-500"
                : "border-yellow-500"
            } bg-slate-200 rounded border text-slate-800 px-2 w-4/5 placeholder:text-slate-800`}
          />
        </div>
        <div className="flex gap-8 justify-center mt-5">
          {inputEducationLength === i && inputEducationLength !== 1 ? (
            <button
              type="button"
              onClick={() => handleDeleteInput(i)}
              className={`bg-slate-700 p-2 rounded-full text-white`}
            >
              <HiOutlineTrash />
            </button>
          ) : null}
          {inputEducationLength < 4 && inputEducationLength === i ? (
            <button
              type="button"
              onClick={() => handleAddInput(i)}
              className={`${
                themeMode
                  ? "border-white text-white"
                  : "border-slate-700 text-slate-700"
              } border  p-2 rounded-full `}
            >
              <HiOutlinePlus />
            </button>
          ) : null}
        </div>
      </div>
    );

    result.push(temp);
  }
  return result;
};

export default InputEducation;
