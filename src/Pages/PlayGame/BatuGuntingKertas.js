import React from "react";
import { useEffect, useState } from "react";
import axios from "axios";
import Cookies from "js-cookie";
import { HiHeart, HiOutlineArrowLeft } from "react-icons/hi2";

import "../../assets/style/rock-paper.css";
import Logo2 from "../../assets/img/logo-2.png";
import Batu from "../../assets/img/batu.png";
import Kertas from "../../assets/img/kertas.png";
import Gunting from "../../assets/img/gunting.png";
import Refresh from "../../assets/img/refresh.png";
import Loading from "../../Components/Loading";
import { Link } from "react-router-dom";
import Modal from "../../Components/Modal";

const BatuGuntingKertas = () => {
  const game_id = 1;
  const [isLoding, setIsLoding] = useState(true);
  const [showModal, setShowModal] = useState(false);
  const [playerChoose, setPlayerChoose] = useState("");
  const [comChoose, setComChoose] = useState("");
  const [resultGame, setResultGame] = useState("");
  const [isPlayerChoose, setIsPlayerChoose] = useState(false);
  const [counterClick, setCounterClick] = useState(true);
  const [buttonRefresh, setButtonRefresh] = useState(false);
  const [life, setLife] = useState(3);
  const [startScore, setStartScore] = useState(0);
  const [higherScore, setHigherScore] = useState(null);
  const [reFetchData, setreFetchData] = useState(true);
  const [backgroundColorComBatu, setBackgroundColorComBatu] =
    useState("transparent");
  const [backgroundColorComKertas, setBackgroundColorComKertas] =
    useState("transparent");
  const [backgroundColorComGunting, setBackgroundColorComGunting] =
    useState("transparent");

  useEffect(() => {
    const fetch = async () => {
      try {
        const dataScore = await axios.post(
          `${process.env.REACT_APP_BASE_URL}/games/play-game`,
          { game_id },
          {
            headers: { Authorization: `${Cookies.get("token")}` },
          }
        );
        setHigherScore(dataScore.data.data.score);
        setIsLoding(false);
      } catch (error) {
        alert(error);
        setIsLoding(false);
      }
    };

    if (reFetchData) {
      fetch();
      setreFetchData(false);
    }
  }, [reFetchData, setreFetchData]);

  useEffect(() => {
    if (resultGame === "You win") {
      setStartScore(startScore + 10);
    }
    if (resultGame === "You Loser") {
      if (life > 1) {
        setLife(life - 1);
      } else {
        setShowModal(true);
        setLife(life - 1);
        if (startScore > higherScore) {
          setIsLoding(true);
          try {
            const postData = async () => {
              const newScore = await axios.put(
                `${process.env.REACT_APP_BASE_URL}/games/update-score`,
                { score: startScore, game_id },
                {
                  headers: { Authorization: `${Cookies.get("token")}` },
                }
              );
              setHigherScore(newScore.data.score);
            };
            postData();
            setreFetchData(true);
          } catch (error) {
            alert(error);
            setIsLoding(false);
          }
        }
      }
    }
  }, [resultGame, setResultGame]);

  const getRandomNumberInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  const rollGame = (player, com) => {
    if (player === com) return `draw`;
    else if (player === `batu`) {
      if (com === `gunting`) return `You win`;
      else return `You Loser`;
    } else if (player === `kertas`) {
      if (com === `batu`) return `You win`;
      else return `You Loser`;
    } else if ((player = `gunting`)) {
      if (com === `kertas`) return `You win`;
      else return `You Loser`;
    }
  };

  const drawingColorCom = () => {
    let randomNumber = getRandomNumberInt(1, 3);
    let randomColor = `rgb(${getRandomNumberInt(0, 255)},${getRandomNumberInt(
      0,
      255
    )},${getRandomNumberInt(0, 255)})`;
    if (randomNumber === 1) {
      setBackgroundColorComBatu(randomColor);
      setBackgroundColorComKertas("transparent");
      setBackgroundColorComGunting("transparent");
    } else if (randomNumber === 2) {
      setBackgroundColorComBatu("transparent");
      setBackgroundColorComKertas(randomColor);
      setBackgroundColorComGunting("transparent");
    } else {
      setBackgroundColorComBatu("transparent");
      setBackgroundColorComKertas("transparent");
      setBackgroundColorComGunting(randomColor);
    }
  };

  const resetColor = () => {
    setBackgroundColorComBatu("transparent");
    setBackgroundColorComKertas("transparent");
    setBackgroundColorComGunting("transparent");
  };

  const comSelect = (com) => {
    if (com === 0) return "batu";
    else if (com === 1) return "kertas";
    else return "gunting";
  };

  const handlerPlayerChoose = (player) => {
    if (counterClick) {
      setCounterClick(false);
      let interval;
      setPlayerChoose(player);
      setIsPlayerChoose(true);
      const com = getRandomNumberInt(0, 2);
      if (!interval) interval = setInterval(drawingColorCom, 50);

      setTimeout(() => {
        clearInterval(interval);
        let comResult = comSelect(com);
        setComChoose(comResult);
        resetColor();
        setResultGame(rollGame(player, comResult));
        setButtonRefresh(true);
      }, 2000);

      setTimeout(() => {
        setCounterClick(true);
        setPlayerChoose("");
        setComChoose("");
        setIsPlayerChoose(false);
        setResultGame("");
        setButtonRefresh(false);
      }, 4000);
    }
  };

  const handleRefresh = () => {
    setCounterClick(true);
    setPlayerChoose("");
    setComChoose("");
    setIsPlayerChoose(false);
    setResultGame("");
    setButtonRefresh(false);
  };

  const handlerButtonModal = () => {
    setShowModal(false);
    setStartScore(0);
    setLife(3);
    setCounterClick(true);
    setPlayerChoose("");
    setComChoose("");
    setIsPlayerChoose(false);
    setResultGame("");
    setButtonRefresh(false);
  };

  const lifeComponents = () => {
    const result = [];
    for (let i = 0; i < life; i++) {
      result.push(
        <div key={i} className="text-red-600">
          <HiHeart color={"#ff0000"} className="w-[30px] h-[30px]" />
        </div>
      );
    }
    return result;
  };

  let lifeIcon = lifeComponents();

  const BodyModal = () => {
    return (
      <div className="text-center text-xl text-white">
        <p>Score permainan anda : {startScore}</p>
        <p>
          Score terbaik anda :{" "}
          {higherScore > startScore ? higherScore : startScore}
        </p>
      </div>
    );
  };

  return (
    <div className="container-game text-white">
      {isLoding && <Loading />}
      {showModal ? (
        <Modal
          body={<BodyModal />}
          title={"Score Anda"}
          footer={
            <>
              <button
                onClick={handlerButtonModal}
                className="bg-green-400 px-3 py-1 rounded hover:bg-green-500 font-semibold"
              >
                Yaay!
              </button>
            </>
          }
          setIsModalView={setShowModal}
        />
      ) : null}
      <section className="flex flex-col relative lg:pt-10 pt-5">
        <div className="header flex gap-5 mb-10">
          <div className="self-center">
            <Link to="/game-list">
              <span>
                <HiOutlineArrowLeft
                  className="w-[50px] h-[50px]"
                  color={"#f59e0b"}
                />
              </span>
            </Link>
          </div>
          <div className="self-center">
            <img src={Logo2} alt="logo2" />
          </div>
          <div className="self-center">
            <strong> ROCK PAPER SCISSORS</strong>
          </div>
        </div>
        {/* CONTENT */}
        <div className="flex justify-between px-3 lg:px-10 mb-10">
          <div className="">
            <div className="flex gap-3">
              <div className="text-xl">Life :</div>
              <div className="flex">
                {life ? <>{lifeIcon.map((el) => el)}</> : null}
              </div>
            </div>
            <p className="text-sm lg:text-2xl">
              Score permainan : {startScore}
            </p>
          </div>
          <div className="text-sm lg:text-2xl">
            <div>
              {Cookies.get("name") ? Cookies.get("name") : "Tidak Diketahui"}
            </div>
            <div>Score tertinggi anda: {higherScore}</div>
          </div>
        </div>
        <div className="content-game flex justify-evenly">
          {/* PLAYER 1 */}
          <div className="flex flex-col">
            <h3 className="text-center text-2xl font-semibold mb-10">You</h3>
            <div
              onClick={() => handlerPlayerChoose("batu")}
              className={`${playerChoose === "batu" ? "player-choose" : ""} ${
                isPlayerChoose ? "" : "icon-hover content-hover"
              } player flex content mb-5 justify-center`}
              data-player="batu"
            >
              <img className="batu self-center" src={Batu} alt="batu" />
            </div>
            <div
              onClick={() => handlerPlayerChoose("kertas")}
              className={`${playerChoose === "kertas" ? "player-choose" : ""} ${
                isPlayerChoose ? "" : "icon-hover content-hover"
              } player content flex mb-5 justify-center`}
              data-player="kertas"
            >
              <img className="self-center" src={Kertas} alt="kertas" />
            </div>
            <div
              onClick={() => handlerPlayerChoose("gunting")}
              className={`${
                playerChoose === "gunting" ? "player-choose" : ""
              } ${
                isPlayerChoose ? "" : "icon-hover content-hover"
              } player content flex mb-5 justify-center`}
              data-player="gunting"
            >
              <img className="self-center" src={Gunting} alt="kertas" />
            </div>
          </div>
          {/* END PLAYER 1 */}
          <div className="flex flex-col justify-center w-1/5">
            {resultGame === "" ? (
              <div className="vs text-center">
                <strong className="">VS</strong>
              </div>
            ) : (
              <div className="result text-center lg:sm:xl:px-8 lg:sm:xl:py-16">
                <strong className="text-result">{resultGame}</strong>
              </div>
            )}
          </div>
          {/* COM */}
          <div className="flex flex-col">
            <h3 className="text-center text-2xl font-semibold mb-10">COM</h3>
            <div
              style={{ backgroundColor: backgroundColorComBatu }}
              className={`com-batu ${
                comChoose === "batu" ? "player-choose" : ""
              } content flex mb-5 justify-center`}
            >
              <img className="batu self-center" src={Batu} alt="batucom" />
            </div>
            <div
              style={{ backgroundColor: backgroundColorComKertas }}
              className={`com-kertas ${
                comChoose === "kertas" ? "player-choose" : ""
              } content flex mb-5 justify-center`}
            >
              <img className="self-center" src={Kertas} alt="kertascom" />
            </div>
            <div
              style={{ backgroundColor: backgroundColorComGunting }}
              className={`com-gunting ${
                comChoose === "gunting" ? "player-choose" : ""
              } content flex mb-5 justify-center`}
            >
              <img className="self-center" src={Gunting} alt="guntingcom" />
            </div>
          </div>
          {/* END COM */}
        </div>
        {/* END CONTENT */}
        {/* REFRESH */}
        {buttonRefresh && (
          <button onClick={handleRefresh} className="refresh absolute">
            <img src={Refresh} alt="refresh" />
          </button>
        )}
        {/* END REFRESH */}
      </section>
    </div>
  );
};

export default BatuGuntingKertas;
