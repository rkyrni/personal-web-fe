import React, { useEffect, useState } from "react";
import axios from "axios";
import Cookies from "js-cookie";
import { HiOutlineArrowLeft } from "react-icons/hi2";
import Loading from "../../Components/Loading";
import { Link } from "react-router-dom";
import Modal from "../../Components/Modal";

const TebakAngka = () => {
  const game_id = 2;
  const [input, setInput] = useState(0);
  const [showModal, setShowModal] = useState(false);
  const [secretNumber, setSecretNumber] = useState(null);
  const [result, setResult] = useState("Mulai menebak angka...");
  const [scoreInGame, setScoreInGame] = useState(200);
  const [isWin, setIsWin] = useState(false);
  const [isLose, setIsLose] = useState(false);
  const [reFetchData, setreFetchData] = useState(true);
  const [higherScore, setHigherScore] = useState(null);
  const [isLoding, setIsLoding] = useState(false);

  const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  useEffect(() => {
    setSecretNumber(getRandomInt(1, 100));
  }, []);

  useEffect(() => {
    const fetch = async () => {
      try {
        const dataScore = await axios.post(
          `${process.env.REACT_APP_BASE_URL}/games/play-game`,
          { game_id },
          {
            headers: { Authorization: `${Cookies.get("token")}` },
          }
        );
        setHigherScore(dataScore.data.data.score);
        setIsLoding(false);
      } catch (error) {
        alert(error);
        setIsLoding(false);
      }
    };

    if (reFetchData) {
      fetch();
      setreFetchData(false);
    }
  }, [reFetchData]);

  const handleCheck = () => {
    console.log(secretNumber);
    if (scoreInGame - 10 === 0) {
      setResult("You lose...");
      setIsLose(true);
      setScoreInGame(0);
      return;
    }

    if (input > secretNumber) {
      setResult("📈 Terlalu Tinggi...");
      setScoreInGame(scoreInGame - 10);
      setShowModal(true);
    } else if (input < secretNumber) {
      setResult("📉 Terlalu Rendah...");
      setScoreInGame(scoreInGame - 10);
      setShowModal(true);
    } else if (input == secretNumber) {
      setResult("🎉 You winn...");
      setIsWin(true);
      if (scoreInGame > higherScore) {
        setIsLoding(true);
        try {
          const postData = async () => {
            const newScore = await axios.put(
              `${process.env.REACT_APP_BASE_URL}/games/update-score`,
              { score: scoreInGame, game_id },
              {
                headers: { Authorization: `${Cookies.get("token")}` },
              }
            );
            setHigherScore(newScore.data.score);
          };
          postData();
          setreFetchData(true);
        } catch (error) {
          alert(error);
          setIsLoding(false);
        }
      }
    }
  };

  const handleAgain = () => {
    setIsLose(false);
    setIsWin(false);
    setResult("Mulai menebak angka...");
    setScoreInGame(200);
    setSecretNumber(getRandomInt(1, 100));
    setInput(0);
  };

  const BodyModal = () => {
    return (
      <div className="text-center text-xl text-white">
        <p className="text-xl">{input}</p>
        <p>{result}</p>
      </div>
    );
  };

  return (
    <>
      <div
        className={`${
          isWin ? "bg-emerald-500" : "bg-black"
        } container-guess-numbers h-screen text-white`}
      >
        {isLoding ? <Loading /> : null}
        {showModal ? (
          <Modal
            body={<BodyModal />}
            footer={
              <>
                <button
                  onClick={() => setShowModal(false)}
                  className="bg-green-400 px-3 py-1 rounded hover:bg-green-500 font-semibold"
                >
                  oke
                </button>
              </>
            }
            setIsModalView={setShowModal}
          />
        ) : null}
        <header
          style={{ height: "36vh" }}
          className={`${
            isWin ? "border-black" : "border-emerald-500"
          } border-b-8 flex justify-between pt-10 lg:px-7 px-2`}
        >
          <div>
            <div className="self-center">
              <Link to="/game-list">
                <HiOutlineArrowLeft
                  color={"#f59e0b"}
                  className="w-[50px] h-[50px]"
                />
              </Link>
            </div>
            <button
              onClick={handleAgain}
              className={`${
                isWin || isLose ? "visible" : "invisible"
              } text-[8px] lg:text-xl bg-white text-black px-2 py-1`}
            >
              Mulai Ulang!
            </button>
          </div>
          <div className="self-center flex flex-col gap-9 -mb-20">
            <h1 className="text-xs lg:text-4xl mb-8 text-center">
              Tebak Angka ku!
            </h1>
            <div className="text-center text-4xl -mb-16 bg-slate-500 py-10 lg:w-2/5 w-3/4 self-center">
              {isWin || isLose ? secretNumber : "?"}
            </div>
          </div>
          <p className="text-[8px] lg:text-xl">(Antara 1 sampai 100)</p>
        </header>
        <main
          style={{ height: "62vh" }}
          className="flex justify-between lg:px-32 px-3 gap-10 lg:gap-0"
        >
          <section className="flex flex-col self-center">
            <input
              type="number"
              value={input}
              onChange={(e) => setInput(e.target.value)}
              className="border-4 border-white text-2xl lg:p-10 p-2 lg:w-60 w-32 text-center block mb-12 font-bold text-slate-800 bg-slate-100"
            />
            <button
              onClick={handleCheck}
              disabled={scoreInGame < 10 || isLose || isWin}
              className="btn bg-white text-black hover:bg-slate-200 py-3"
            >
              Check!
            </button>
          </section>
          <section className="self-center">
            <p className="mb-10 text-[8px] lg:text-xl">{result}</p>
            <p className="mb-4 text-[8px] lg:text-xl">
              😉{" "}
              <span className="score">
                {Cookies.get("name") ? Cookies.get("name") : "Tidak Diketahui"}
              </span>
            </p>
            <p className="mb-4 text-[8px] lg:text-xl">
              💯 Score in game: <span className="score">{scoreInGame}</span>
            </p>
            <p className="text-[8px] lg:text-xl">
              🥇 Highscore: <span className="highscore">{higherScore}</span>
            </p>
          </section>
        </main>
      </div>
    </>
  );
};

export default TebakAngka;
