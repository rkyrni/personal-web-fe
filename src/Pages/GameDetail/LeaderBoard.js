import axios from "axios";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { IoMdTrophy } from "react-icons/io";

const LeaderBoard = ({ game_id, setIsLoading }) => {
  const [fetchData, setFetchData] = useState(true);
  const [dataLeaderboard, setDataLeaderboard] = useState(null);
  const [users, setUsers] = useState(null);

  const themeMode = useSelector((state) => state.themeMode.value);

  useEffect(() => {
    const fetch = async () => {
      setIsLoading(true);
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_BASE_URL}/leaderboard/${game_id}`
        );

        const responseUsers = await axios.get(
          `${process.env.REACT_APP_BASE_URL}/users`
        );

        setUsers(responseUsers.data.data);
        setDataLeaderboard(response.data.data);
        setIsLoading(false);
      } catch (error) {
        alert(error);
        setIsLoading(false);
      }
    };

    if (fetchData) {
      fetch();
      setFetchData(false);
    }
  }, [fetchData]);

  const handlingChar = (param) => {
    let result = "";
    for (let j = 0; j < param.length; j++) {
      if (j < 11) result += param[j];
      if (j === 11) {
        result += "...";
        break;
      }
    }
    return result;
  };

  const ContentLeaderboard = () => {
    return (
      <>
        {dataLeaderboard ? (
          <>
            {dataLeaderboard.map((item, i) => {
              let styleRank = "bg-slate-200 py-3 px-5";
              if (i === 0) styleRank = "bg-yellow-500 py-6 px-5";
              if (i === 1) styleRank = "bg-green-500 py-5 px-5";
              if (i === 2) styleRank = "bg-blue-500 py-4 px-5";
              if (i > 5) return false;
              return (
                <div key={i} className={`${styleRank} rounded-lg shadow-lg `}>
                  <div className="flex justify-center gap-5">
                    {i === 0 ? (
                      <span>
                        <IoMdTrophy className="text-red-600 w-[30px] h-[30px]" />
                      </span>
                    ) : null}
                    {users ? (
                      <>
                        {users.map((el, j) => {
                          if (el.id === item.user_id) {
                            return (
                              <p key={j} className="text-black font-bold">
                                {handlingChar(el.name)} :
                              </p>
                            );
                          }
                          return false;
                        })}
                      </>
                    ) : null}

                    <p className="text-xl font-bold text-black">{item.score}</p>
                  </div>
                </div>
              );
            })}
          </>
        ) : null}
      </>
    );
  };

  return (
    <div
      className={`${
        themeMode
          ? "bg-slate-800 text-white shadow-md shadow-white"
          : "bg-white text-slate-800 shadow-lg"
      } rounded-lg`}
    >
      <div>
        <h2 className="text-center text-2xl text-yellow-500 font-bold">
          Papan Peringkat
        </h2>
      </div>
      <div className="flex flex-col gap-2 px-4 py-2">
        <ContentLeaderboard />
      </div>
    </div>
  );
};

export default LeaderBoard;
