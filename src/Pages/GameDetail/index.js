import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Link, useNavigate, useParams } from "react-router-dom";
import Footer from "../../Components/Footer";
import Loading from "../../Components/Loading";
import Navbar from "../../Components/Navbar";
import LeaderBoard from "./LeaderBoard";

const GameDetail = () => {
  const [fetchData, setFetchData] = useState(true);
  const [gameDetail, setGameDetail] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const params = useParams();
  const navigate = useNavigate();
  const themeMode = useSelector((state) => state.themeMode.value);

  useEffect(() => {
    const fetch = async () => {
      setIsLoading(true);
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_BASE_URL}/game-detail/${params.label}`
        );
        setGameDetail(response.data.data);
        setIsLoading(false);
      } catch (error) {
        alert(error);
        setIsLoading(false);
      }
    };

    if (fetchData) {
      fetch();
      setFetchData(false);
    }
  }, [fetchData, params.label]);

  return (
    <div>
      {isLoading ? <Loading /> : null}
      <Navbar />
      <div
        className={`${
          themeMode ? "bg-black text-slate-200" : "bg-slate-200 text-slate-800"
        } min-h-screen pt-16`}
      >
        <div className="flex flex-col gap-7">
          <div className="flex flex-col gap-5 p-2">
            <div>
              <h2 className="text-center text-2xl text-yellow-500 font-bold">
                {gameDetail?.name}
              </h2>
            </div>
            <div
              className={`${
                themeMode
                  ? "bg-slate-800 shadow-md shadow-white text-slate-200"
                  : "bg-white shadow-lg text-slate-800"
              } flex flex-col gap-5 rounded-lg p-2`}
            >
              <img
                className="h-[200px] self-center mb-5"
                src={gameDetail?.image_url}
                alt="gameDetail"
              />
              <p className="text-xs text-center">{gameDetail?.description}</p>
              <div className="flex justify-end gap-6">
                <button
                  onClick={() => navigate(`/game-list`)}
                  type="button"
                  className="text-xs bg-red-500 px-2 py-1 border border-white rounded-lg text-white font-semibold"
                >
                  kembali
                </button>
                <button
                  onClick={() => {
                    if (!Cookies.get("token")) {
                      navigate("/login");
                      return;
                    }
                    navigate(`/play-game/${gameDetail?.label}`);
                  }}
                  type="button"
                  className="text-xs bg-blue-500 px-2 py-1 border border-white rounded-lg text-white font-semibold"
                >
                  mainkan game
                </button>
              </div>
            </div>
          </div>
          <div className="p-2 mb-4">
            {gameDetail?.id ? (
              <LeaderBoard
                game_id={gameDetail?.id}
                setIsLoading={setIsLoading}
              />
            ) : null}
          </div>
        </div>
        <Footer />
      </div>
    </div>
  );
};

export default GameDetail;
