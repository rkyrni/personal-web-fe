import React from "react";
import BackOfficeTemplate from "../../layouts/BackOffice";

const HomeBo = () => {
  return (
    <BackOfficeTemplate>
      <div>Home Back Office</div>
    </BackOfficeTemplate>
  );
};

export default HomeBo;
