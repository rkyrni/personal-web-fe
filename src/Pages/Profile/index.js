import React, { useEffect, useState } from "react";
import Navbar from "../../Components/Navbar";
import DefaultProfile from "../../assets/img/default-profile.png";
import axios from "axios";
import Cookies from "js-cookie";
import Loading from "../../Components/Loading";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const Profile = () => {
  const [user, setUser] = useState(null);
  const [fecth, setFetch] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const themeMode = useSelector((state) => state.themeMode.value);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_BASE_URL}/user`,
          {
            headers: { Authorization: `${Cookies.get("token")}` },
          }
        );
        setUser(response.data);
        setIsLoading(false);
      } catch (error) {
        alert(error);
        setIsLoading(false);
      }
    };

    if (fecth) {
      fetchData();
      setFetch(false);
    }
  }, [fecth, user]);

  return (
    <div>
      <Navbar />
      {isLoading ? <Loading /> : null}
      <div
        className={`${
          themeMode ? "bg-black text-slate-200" : "bg-white text-slate-800"
        } p-2`}
      >
        <div
          className={`${
            themeMode
              ? "bg-slate-800 shadow-md shadow-white"
              : "bg-slate-100 shadow-lg"
          } rounded flex flex-col pt-16 lg:pt-24 min-h-screen gap-5`}
        >
          <div className="self-center flex flex-col gap-5 mb-4">
            <img
              src={user?.imageURL ? user.imageURL : DefaultProfile}
              alt="profile"
              className="w-[100px] h-[100px] lg:w-[200px] lg:h-[200px] self-center rounded-lg"
            />
            <h2 className="text-center font-semibold lg:text-2xl">
              {user?.name}
            </h2>
          </div>
          <div className="px-10">
            <p className="text-xs lg:text-sm lg:text-center">{user?.bio}</p>
          </div>
          <div className="flex justify-end pr-6 lg:pr-96">
            <Link
              to="/edit-profile"
              type="button"
              className={`${
                themeMode ? "border border-white" : "border border-black"
              } rounded-lg py-1 px-2 text-xs`}
            >
              edit profile
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Profile;
